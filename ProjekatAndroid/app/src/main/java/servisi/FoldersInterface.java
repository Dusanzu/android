package servisi;

import java.util.ArrayList;

import model.Folder;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface FoldersInterface {
    @GET("folders/account/{username}")
    Call<ArrayList<Folder>> getAllFolders(@Path("username") String username);

    @GET("folders/{id}")
    Call<Folder> getFolder(@Path("id") int id);

    @DELETE("folders/{id}")
    Call<Void> getDeleteFolders(@Path("id") int id);

    @PUT("folders/{id}")
    Call<Folder> updateFolders(@Body Folder folder, @Path("id") int id);

    @POST("folders/{username}/{subject}")
    Call<Void> addNewFolder(@Body Folder folder, @Path("username") String username,
                            @Path("subject") String subject);
}
