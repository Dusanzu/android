package servisi;

import java.util.List;
import java.util.ArrayList;

import model.Account;
import model.Folder;
import model.Message;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;


public interface AcountService {
    @GET("accounts/login/{username}/{password}")
    Call<Account> login(@Path("username") String username, @Path("password") String password);

    @GET("accounts/getallaccount/{username}")
    Call<ArrayList<Account>> getAllAccount(@Path("username") String username);


//    @GET("accounts")
//    Call<ArrayList<Account>> getAllAccounts();
//
//    @FormUrlEncoded
//    @POST("login")
//    Call <Void> userLogin(@Field("username") String username, @Field("password") String password);
//
//    /**
//     * Logout
//     */
//    @POST("logout")
//    Call<Void> logout();
@PUT("accounts/addAccaunt/{username}")
Call<Void> addAccaunt(@Body Account account ,@Path("username") String username);
@PUT("accounts/registrationUser/{username}/{password}/{name}/{lastname}")
Call<Void> registrationUser(@Path("username") String username, @Path("password") String password, @Path("name") String name, @Path("lastname") String lastname);

    @PUT("accounts/{id}")
    Call<Account> editAccount(@Body Account account, @Path("id") int id);


    @GET("accounts/username/{username}")
    Call<Account> getAccount(@Path("username") String username);
}
