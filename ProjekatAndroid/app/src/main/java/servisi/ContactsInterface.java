package servisi;

import java.util.List;

import model.Contact;
import model.Message;
import model.Photo;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ContactsInterface {
    @GET("contacts/account/{username}")
    Call<List<Contact>> getAllContacts(@Path("username") String username);

    @POST("contacts/{username}")
    Call<Void> addContact(@Body Contact contact, @Path("username") String username);

    @DELETE("contacts/{id}")
    Call<Void> getDeleteContact(@Path("id") int id);


    @PUT("contacts/{id}/{username}")
    Call<Contact> addUpdateContact(@Body Contact contact, @Path("id") int id,@Path("username") String username);

    @GET("contacts/emails/account/{username}")
    Call<List<String>> getAllEmails(@Path("username") String username);
}
