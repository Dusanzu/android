package servisi;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.example.projekatandroid.LoginActivity;
import com.example.projekatandroid.R;

import model.Account;

//import static com.anuragdhunna.www.sessionmangementsharedpref.utils.PreferencesUtility.*;



public class SaveSharedPreference {

    public static SharedPreferences getPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    /**
     * Set the Login Status
     * @param context

     */
    public static void setLoggedIn(Context context, Account a) {
        SharedPreferences.Editor editor = getPreferences(context).edit()
                .putString("username", a.getToken());
//        editor.putBoolean(LOGGED_IN_PREF, loggedIn);
        editor.commit();
        Log.d("LOGIN",

                        " useraname: " + a.getUsername() +
                        " id: " + a.getId());
    }

    /**
     * Get the Login Status
     * @param context
     * @return boolean: login status
     */
    public static void getLoggedStatus(Context context) {
        SharedPreferences.Editor editor= getPreferences(context).edit().putString(context.getString(R.string.current_user_name),null);
        editor.commit();
        context.startActivity(new Intent(context, LoginActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
    }


    public static void setRefreshrate(Context context, String refreshrate){
        SharedPreferences.Editor editor = getPreferences(context).edit()
                .putString("refresh_rate", refreshrate);
    }


}
