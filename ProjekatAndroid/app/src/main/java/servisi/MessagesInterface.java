package servisi;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import model.Account;
import model.Attachment;
import model.Folder;
import model.Message;
import model.Tag;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface MessagesInterface {
    @GET("messages/account/{username}")
    Call<ArrayList<Message>> getAllMessages(@Path("username") String username);

    @GET("messages/accountLike/{username}")
    Call<ArrayList<Message>> getAllMessagesLikes(@Path("username") String username);

    @GET("messages/accountFavorite/{username}")
    Call<ArrayList<Message>> getAllMessagesFavorte(@Path("username") String username);

    @GET("messages/account/sortAsc/{username}")
    Call<ArrayList<Message>> getAllMessagesAsc(@Path("username") String username);

    @GET("messages/account/sortDes/{username}")
    Call<ArrayList<Message>> getAllMessagesDesc(@Path("username") String username);

    @DELETE("messages/{id}")
    Call<Void> getDeleteMessages(@Path("id") int id);


    @PUT("messages/viewed/{id}")
    Call<Void> getviewedMessages(@Path("id") int id);

    @PUT("messages/tag/{messageId}/{tagName}/{add}")
    Call<Void> getMessages(@Path("messageId") int messageId,
                           @Path("tagName") String tagName, @Path("add") boolean add);

    @POST("messages/addToDraft/{username}")
    Call<Message> addToDraft(@Body Message message , @Path("username") String username);

    @POST("messages/send/{username}")
    Call<Message> send(@Body Message message, @Path("username") String username);

    @POST("messages/send-email")
    Call<Void> sendEmail(@Body Message message);


}
