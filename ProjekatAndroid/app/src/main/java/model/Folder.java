package model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Folder implements Serializable {
    private int id;
    private String name;
    private List<Folder> folders;
    private Rule rule;
    private List<Message> messages;

    public Folder(){}

    public Folder(int id, String name, List<Folder> folders, Rule rule, List<Message> messages){
        this.id = id;
        this.name = name;
        this.folders = folders;
        this.rule = rule;
        this.messages = messages;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Folder> getFolders() {
        return folders;
    }

    public void setFolders(List<Folder> folders) {
        this.folders = folders;
    }

    public Rule getRule() {
        return rule;
    }

    public void setRule(Rule rule) {
        this.rule = rule;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessage(List<Message> messages) {
        this.messages = messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }


}
