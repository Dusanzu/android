package model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Contact implements Serializable {
    public enum Format {
        PLAIN,
        HTML
    }
    private int id;
    private String first;
    private String last;
    private String display;
    private String email;
    private Format format;
    private Photo photo;

    public Contact() {
    }

    public Contact(int id, String first, String last, String display, String email,
                   Format format, Photo photo) {
        this.id = id;
        this.first = first;
        this.last = last;
        this.display = display;
        this.email = email;
        this.format = format;
        this.photo = photo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public String getLast() {
        return last;
    }

    public void setLast(String last) {
        this.last = last;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Format getFormat() {
        return format;
    }

    public void setFormat(Format format) {
        this.format = format;
    }

    public Photo getPhoto() {
        return photo;
    }

    public void setPhoto(Photo photo) {
        this.photo = photo;
    }
}
