package model;

import java.io.Serializable;
import java.util.List;

public class Tag implements Serializable {
    private int id;
    private String name;

    public Tag(){}

    public Tag(int id, String name, List<Message> messages) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



}
