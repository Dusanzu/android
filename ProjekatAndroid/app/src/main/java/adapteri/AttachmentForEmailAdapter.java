package adapteri;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.projekatandroid.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import model.Attachment;


public class AttachmentForEmailAdapter extends ArrayAdapter<Attachment> {

    List<Attachment> attachments;
    Context mContext;
    public AttachmentForEmailAdapter(Context context, int textViewResourceId, List<Attachment> objects) {
        super(context, textViewResourceId, objects);
        attachments= objects;
        mContext=context;
    }
    @Override
    public int getCount() {

        return attachments.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.adapter_view_attachment,parent,false);

        Attachment currentMovie = attachments.get(position);
        TextView ime = (TextView)listItem.findViewById(R.id.name);
        TextView tip = (TextView) listItem.findViewById(R.id.type);
        ime.setText( attachments.get(position).getName());
        tip.setText( attachments.get(position).getType());
        return listItem;
    }

}
