package adapteri;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.projekatandroid.R;

import java.util.List;

import model.Folder;
import model.Message;
import model.Rule;

public class FoldersAdapter extends ArrayAdapter<Folder> {

    private Context mContext;
    int mResource;

    public FoldersAdapter(Context context, int resource, List<Folder> objects) {
        super(context, resource, objects);
        mContext = context;
        mResource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView,  ViewGroup parent) {
        int id = getItem(position).getId();
        String name = getItem(position).getName();
        Rule rule = getItem(position).getRule();
        List<Folder> folders = getItem(position).getFolders();
        List<Message> messages = getItem(position).getMessages();


        LayoutInflater inflater= LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource, parent, false);
        TextView tvFolderNamee = (TextView) convertView.findViewById(R.id.tvFolderNamee);
        TextView tvCondition = (TextView) convertView.findViewById(R.id.tvCondition);
        TextView tvOperation = (TextView) convertView.findViewById(R.id.tvOperation);

        tvFolderNamee.setText(name);
        tvCondition.setText(rule.getCondition().toString());
        tvOperation.setText(rule.getOperation().toString());

        return convertView;
    }

}
