package adapteri;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;


import android.widget.TextView;


import com.example.projekatandroid.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;


import model.Message;


import static android.content.Context.MODE_PRIVATE;

public class EmailAdapter extends ArrayAdapter<Message> {


    public static int brojac=0;


    public ArrayList<Message> emailList = new ArrayList<>();
    Context mContext;
    public EmailAdapter(Context context, int textViewResourceId, ArrayList<Message> objects) {
        super(context, textViewResourceId, objects);
        emailList= objects;
        mContext=context;
    }
    @Override
    public int getCount() {

        return emailList.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.recyclerview_mail_item,parent,false);
        Message currentMovie = emailList.get(position);
        Log.d("currentMovie1"," doInB1ackground +++++ + "+emailList.get(position));
        TextView posiljalac = (TextView)listItem.findViewById(R.id.tvEmailSender);
        TextView naslov = (TextView) listItem.findViewById(R.id.tvEmailTitle);
        TextView poruka = (TextView) listItem.findViewById(R.id.tvEmailDetails);
        TextView vreme = (TextView) listItem.findViewById(R.id.tvEmailTime);

        SharedPreferences.Editor editor = this.getContext().getSharedPreferences("pogledano", MODE_PRIVATE).edit();
        editor.putString("posiljalac",emailList.get(position).getFrom());
        editor.putString("poruka",emailList.get(position).getContent());
        editor.putString("naslov",emailList.get(position).getSubject());

        editor.putString("pozicija8",String.valueOf(emailList.get(position)));

        posiljalac.setText( emailList.get(position).getFrom());
        naslov.setText( emailList.get(position).getSubject() );
        poruka.setText( emailList.get(position).getContent() );
        vreme.setText(toISO8601UTC( emailList.get(position).getDateTime()));

        if(emailList.get(position).isProcitano()==false) {
            Log.d("promena","1doInB1ackground +++++ + "+emailList.get(position).isProcitano());
            brojac++;
            Log.d("brojac", String.valueOf(brojac));
            if(brojac>2){
                Log.d("brojac", String.valueOf(brojac));
                Log.d("unutar", String.valueOf(brojac));
                editor.putString("position",String.valueOf(position));
                editor.putString("posiljalac","vise");
                editor.putString("poruka",String.valueOf(brojac));
                editor.putString("naslov","brojac");

            }


            if(brojac==0){
                Log.d("brojac12", String.valueOf(brojac));
                editor.putString("naslov","zaustavi");
            }
            editor.apply();
           TextView me = (TextView) listItem.findViewById(R.id.tvEmailSender);
           me.setTextColor(Color.RED);
           TextView me1 = (TextView) listItem.findViewById(R.id.tvEmailTitle);
           me1.setTextColor(Color.RED);
           TextView me2 = (TextView) listItem.findViewById(R.id.tvEmailDetails);
           me2.setTextColor(Color.RED);
           TextView me3 = (TextView) listItem.findViewById(R.id.tvEmailTime);
           me3.setTextColor(Color.RED);




       }












//        brojac=0;

        Log.d("brojac1", String.valueOf(brojac));

        Log.d("promena"," doInBad2ckground +++++ + ");
        TextView iconView =(TextView)listItem.findViewById(R.id.tvIcon);
        iconView.setText(emailList.get(position).getFrom().substring(0,1));

        Random mRandom = new Random();
        int color = Color.argb(255, mRandom.nextInt(256), mRandom.nextInt(256), mRandom.nextInt(256));
        ((GradientDrawable)iconView.getBackground()).setColor(color);

        return listItem;
    }

    public void update(List<Message>list){
        emailList=new ArrayList<>();
        emailList.addAll(list);
        notifyDataSetChanged();
    }


    public static String toISO8601UTC(Date date) {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");
        df.setTimeZone(tz);
        return df.format(date);
    }


}



