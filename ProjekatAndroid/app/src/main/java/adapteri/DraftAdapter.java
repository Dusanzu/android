package adapteri;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.projekatandroid.R;

import java.util.ArrayList;
import java.util.Random;

import model.Message;

import static android.content.Context.MODE_PRIVATE;

public class DraftAdapter extends ArrayAdapter<Message> {

    public ArrayList<Message> emailList = new ArrayList<>();
    Context mContext;
    public static int brojac=0;
    public DraftAdapter(Context context, int textViewResourceId, ArrayList<Message> objects) {
        super(context, textViewResourceId, objects);
        emailList= objects;
        mContext=context;
    }

    @Override
    public int getCount() {

        return emailList.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.draft,parent,false);
        Message currentMovie = emailList.get(position);
        Log.d("currentMovie1"," doInB1ackground +++++ + "+emailList.get(position));
        TextView posiljalac = (TextView)listItem.findViewById(R.id.tvEmailSenderDraft);
        TextView naslov = (TextView) listItem.findViewById(R.id.tvEmailTitleDraft);
        TextView poruka = (TextView) listItem.findViewById(R.id.tvEmailDetailsDraft);
        TextView vreme = (TextView) listItem.findViewById(R.id.tvEmailTimeDraft);

        SharedPreferences.Editor editor = this.getContext().getSharedPreferences("pogledano", MODE_PRIVATE).edit();
        editor.putString("posiljalac",emailList.get(position).getFrom());
        editor.putString("poruka",emailList.get(position).getContent());
        editor.putString("naslov",emailList.get(position).getSubject());

        editor.putString("pozicija8",String.valueOf(emailList.get(position)));

        posiljalac.setText( emailList.get(position).getFrom());
        naslov.setText( emailList.get(position).getSubject() );
        poruka.setText( emailList.get(position).getContent() );
        vreme.setText( emailList.get(position).getDateTime().toString());
//


        Log.d("promena55"," doInB1ackground +++++ + "+position);


        if(emailList.get(position).isProcitano()==false) {
            Log.d("promena"," 1doInB1ackground +++++ + "+emailList.get(position).isProcitano());
            brojac++;
            Log.d("brojac", String.valueOf(brojac));
            if(brojac>1){
                Log.d("brojac", String.valueOf(brojac));

                editor.putString("position",String.valueOf(position));
                editor.putString("posiljalac","pristigle poruke");
                editor.putString("poruka",String.valueOf(brojac));
                editor.putString("naslov","brojac");

            }

            editor.apply();



            TextView me = (TextView) listItem.findViewById(R.id.tvEmailSenderDraft);
            me.setTextColor(Color.RED);
            TextView me1 = (TextView) listItem.findViewById(R.id.tvEmailTitleDraft);
            me1.setTextColor(Color.RED);
            TextView me2 = (TextView) listItem.findViewById(R.id.tvEmailDetailsDraft);
            me2.setTextColor(Color.RED);
            TextView me3 = (TextView) listItem.findViewById(R.id.tvEmailTimeDraft);
            me3.setTextColor(Color.RED);




        }
        Log.d("brojac1", String.valueOf(brojac));

        Log.d("promena"," doInBad2ckground +++++ + ");
        TextView iconView =(TextView)listItem.findViewById(R.id.tvIcon);
        iconView.setText(emailList.get(position).getFrom().substring(0,1));

        Random mRandom = new Random();
        int color = Color.argb(255, mRandom.nextInt(256), mRandom.nextInt(256), mRandom.nextInt(256));
        ((GradientDrawable)iconView.getBackground()).setColor(color);

        return listItem;
    }

    }
