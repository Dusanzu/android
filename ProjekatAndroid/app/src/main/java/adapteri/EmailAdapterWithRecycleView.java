package adapteri;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.projekatandroid.R;

import java.util.ArrayList;
import java.util.List;

import model.Message;

public class EmailAdapterWithRecycleView extends RecyclerView.Adapter<EmailAdapterWithRecycleView.MyViewHolder> {

    List<Message> emailList;
    public TextView posiljalac, naslov, poruka,vreme;
    public EmailAdapterWithRecycleView(List<Message> emailList) {
        this.emailList = emailList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        public MyViewHolder(View view) {
            super(view);
             posiljalac = (TextView) view.findViewById(R.id.tvEmailSender);
             naslov = (TextView) view.findViewById(R.id.tvEmailTitle);
             poruka = (TextView) view.findViewById(R.id.tvEmailDetails);
             vreme = (TextView) view.findViewById(R.id.tvEmailTime);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_mail_item, parent, false);

        return new MyViewHolder(itemView);
    }
    public void onBindViewHolder(MyViewHolder holder,int position){
//        Message movie = moviesList.get(position);
        posiljalac=holder.itemView.findViewById(R.id.tvEmailSender);
        posiljalac.setText( emailList.get(position).getFrom());

        naslov = holder.itemView.findViewById(R.id.tvEmailTitle);
        poruka = holder.itemView.findViewById(R.id.tvEmailDetails);
        vreme = holder.itemView.findViewById(R.id.tvEmailTime);

        naslov.setText( emailList.get(position).getSubject() );
        poruka.setText( emailList.get(position).getContent() );
        vreme.setText( emailList.get(position).getDateTime().toString());
    }

    @Override
    public int getItemCount() {
        return emailList.size();
    }


}
