package adapteri;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.projekatandroid.R;
import com.squareup.picasso.Picasso;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URI;
import java.util.List;

import model.Contact;
import model.Message;
import model.Photo;

public class ContactAdapter extends ArrayAdapter<Contact> {

    private Context mContext;
    int mResource;

    public ContactAdapter(Context context, int resource, List<Contact> objects) {
        super(context, resource, objects);
        mContext = context;
        mResource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String korisnickoIme = getItem(position).getFirst();

        LayoutInflater inflater= LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource, parent, false);
       ImageView img = (ImageView) convertView.findViewById(R.id.imageView51);

//
        TextView textForName = (TextView) convertView.findViewById(R.id.yy);



//           Uri imageUri =Uri.parse(getItem(position).getPhoto().getPath());
//        InputStream imageStream = getContentResolver().openInputStream(putanja);
//        final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
            Picasso.get().load(getItem(position).getPhoto().getPath()).into(img);

//        img.setImageURI(imageUri);

        textForName.setText(korisnickoIme);



        return convertView;

    }

}
