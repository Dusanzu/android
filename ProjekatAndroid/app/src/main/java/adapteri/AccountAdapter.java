package adapteri;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.projekatandroid.R;

import java.util.List;

import model.Account;
import model.Folder;
import model.Message;
import model.Rule;

public class AccountAdapter extends ArrayAdapter<Account> {
    private Context mContext;
    int mResource;

    public AccountAdapter(Context context, int resource, List<Account> objects) {
        super(context, resource, objects);
        mContext = context;
        mResource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        int id = getItem(position).getId();
        String name = getItem(position).getPassword();





        LayoutInflater inflater= LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource, parent, false);
        TextView tvFolderNamee = (TextView) convertView.findViewById(R.id.naslovzaAccount);


        tvFolderNamee.setText(name);


        return convertView;
    }

}
