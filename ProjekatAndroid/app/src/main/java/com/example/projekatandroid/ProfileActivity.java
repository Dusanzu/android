package com.example.projekatandroid;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.Map;

import model.Account;
import model.Message;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import servisi.AcountService;
import servisi.RetrofitClient;
import servisi.SaveSharedPreference;

public class ProfileActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout drawer;
    private ActionBarDrawerToggle toggle;
    private NavigationView navView;
    private Account account;
    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        Toolbar tool = findViewById(R.id.toolbar_profile);
        setSupportActionBar(tool);

        getSupportActionBar().setTitle("Profile");

        drawer = findViewById(R.id.drawer_profile);

        toggle = new ActionBarDrawerToggle(this,drawer,tool,R.string.drawable_layout_otvori,R.string.drawable_layout_zatvori);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navView = findViewById(R.id.nav_profile);
        navView.setNavigationItemSelectedListener(this);

        View header=navView.getHeaderView(0);
        header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.closeDrawers();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences s = SaveSharedPreference.getPreferences(getApplicationContext());

        String userName = s.getString("username", "abc");
        //
        TextView usernameViewProfile = (TextView)findViewById(R.id.usernameViewProfile);
        TextView passwordViewProfile = findViewById(R.id.passwordViewProfile);

        TextView androidPop3Imap = findViewById(R.id.androidPop3Imap);
        TextView smtpViewProfile = findViewById(R.id.smtpViewProfile);
        Button btnEditProfile = findViewById(R.id.btnEditProfile);
        //
        AcountService acountService = RetrofitClient.getClient().create(AcountService.class);
        Call<Account> getMyAccount = acountService.getAccount(userName);
        getMyAccount.enqueue(new Callback<Account>() {
            @Override
            public void onResponse(Call<Account> call, Response<Account> response) {
                account = response.body();


                usernameViewProfile.setText(account.getUsername());


                passwordViewProfile.setText(account.getPassword());

                androidPop3Imap.setText(account.getPop3Imap());

                smtpViewProfile.setText(account.getSmtp());



                btnEditProfile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(ProfileActivity.this, EditProfileActivity.class);
                        intent.putExtra("PROFILE", account);
                        startActivity(intent);
                    }
                });

            }

            @Override
            public void onFailure(Call<Account> call, Throwable t) {
                Toast.makeText(ProfileActivity.this, "Nije uspelo", Toast.LENGTH_LONG);
            }
        });


//        account = new Account(1, "pera", "djoka", "Djoka",
//                "Babic",  new ArrayList<Message>());



    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.profile_toolbar, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id=item.getItemId();

        if (id == R.id.btnLogoutProfile){
            Toast.makeText(ProfileActivity.this, "Logout", Toast.LENGTH_LONG).show();
            Intent login = new Intent(ProfileActivity.this, LoginActivity.class);
            startActivity(login);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {

            case R.id.profile: {
                Intent profile = new Intent(ProfileActivity.this,ContactsActivity.class);
                startActivity(profile);
                break;
            }
            case R.id.folders: {
                Intent folders = new Intent(ProfileActivity.this, FoldersActivity.class);
                startActivity(folders);
                break;
            }
            case R.id.settings: {
                Intent seti = new Intent(ProfileActivity.this, SettingsActivity.class);
                startActivity(seti);
                break;
            }
            case R.id.emails: {
                Intent emails = new Intent(ProfileActivity.this, EmailsActivity.class);
                startActivity(emails);
                break;
            }





        }
        return true;
    }
}
