package com.example.projekatandroid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import adapteri.EmailAdapter;
import adapteri.FoldersAdapter;
import etc.Utility;
import model.Folder;
import model.Message;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import servisi.FoldersInterface;
import servisi.MessagesInterface;
import servisi.RetrofitClient;

public class FolderActivity extends AppCompatActivity {

    private List<Folder> folders;
    private Folder folder;
    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_folder);

        Toolbar toolbar = findViewById(R.id.toolbarFolders);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Folder");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onResume() {
        super.onResume();

        TextView textViewForFolder = findViewById(R.id.textViewForFolder);
        TextView textViewForFolderCondition = findViewById(R.id.textViewForFolderCondition);
        TextView textViewForFolderOperation = findViewById(R.id.textViewForFolderOperation);

        Intent izFoldersa = getIntent();


        folder = (Folder) izFoldersa.getExtras().getSerializable("FOLDER");

        if (!folder.getName().equals("Drafts") && !folder.getName().equals("Inbox") &&
                !folder.getName().equals("Sent")){
            Toast.makeText(FolderActivity.this , "Ne moze da se menja niti brise ovaj folder", Toast.LENGTH_SHORT).show();
        }

        textViewForFolder.setText(folder.getName());
        textViewForFolderCondition.setText(folder.getRule()
        .getCondition().toString());
        textViewForFolderOperation.setText(folder.getRule().getOperation().toString());
        folders = folder.getFolders();
        //todo odavde
        FoldersAdapter foldersAdapter = new FoldersAdapter(this, R.layout.adapter_view_folders, folder.getFolders());

        ListView listViewFolders = findViewById(R.id.listViewFolderFolder);
        listViewFolders.setAdapter(foldersAdapter);
        Utility.setListViewHeightBasedOnChildren(listViewFolders);

        listViewFolders.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                Intent folder = new Intent(FolderActivity.this, FolderActivity.class);
                folder.putExtra("FOLDER", folders.get(position));

                startActivity(folder);
            }
        });


        //todo otkomentarisi ako bude trazio
        ListView listViewEmails = findViewById(R.id.listViewEmailFolder);
        EmailAdapter emailAdapter=new EmailAdapter(FolderActivity.this,folder.getMessages().size(),(ArrayList<Message>)folder.getMessages());
        listViewEmails.setAdapter(emailAdapter);

        Utility.setListViewHeightBasedOnChildren(listViewEmails);

        if (!folder.getName().equals("Drafts")){
            listViewEmails.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent setings = new Intent(FolderActivity.this, EmailActivity.class);
                    setings.putExtra("EMAIL", folder.getMessages().get(position));
                    startActivity(setings);
                }
            });

        } else{
            listViewEmails.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent setings = new Intent(FolderActivity.this, CreateEmailActivity.class);
                    setings.putExtra("DRAFT_EMAIL", folder.getMessages().get(position));
                    setings.putExtra("NE_MENJAJ", true);
                    startActivity(setings);
                }
            });
        }



        //todo dovde


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.folder_toolbar, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id=item.getItemId();
        if(id==R.id.btnEditFolder){
            if (!folder.getName().equals("Drafts") && !folder.getName().equals("Inbox") &&
                    !folder.getName().equals("Sent")) {
                Toast.makeText(FolderActivity.this, "Novi folder", Toast.LENGTH_LONG).show();
                boolean izmena = true;
                Intent profile = new Intent(FolderActivity.this, CreateFolderActivity.class);
                profile.putExtra("IZMENA", izmena);
                profile.putExtra("FOLDER", folder);
                startActivity(profile);
            }
        }
        if (id == android.R.id.home ){
            finish();
        }
        if(id==R.id.btndeleteFolder){
            Intent izFoldersa = getIntent();
            Folder folder = (Folder) izFoldersa.getExtras().getSerializable("FOLDER");
            if (!folder.getName().equals("Drafts") && !folder.getName().equals("Inbox") &&
            !folder.getName().equals("Sent")){
                FoldersInterface service = RetrofitClient.getClient().create(FoldersInterface.class);
                Call<Void> call = service.getDeleteFolders(folder.getId());
                call.enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        Intent i=new Intent(FolderActivity.this, FoldersActivity.class);
                        startActivity(i);
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        Toast.makeText(FolderActivity.this, "Something went wrong...Please try later!", Toast.LENGTH_SHORT);
                    }
                });
            } else {
                Toast.makeText(FolderActivity.this, "Ne moze se brisati drafts folder",
                        Toast.LENGTH_LONG);
            }


//            Toast.makeText(EmailActivity.this, "Kliknuli ste delete dugme EmailActivity", Toast.LENGTH_LONG).show();
        }


        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
