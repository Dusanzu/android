package com.example.projekatandroid;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import adapteri.AccountAdapter;
import adapteri.FoldersAdapter;
import model.Account;
import model.Folder;
import model.Message;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import servisi.AcountService;
import servisi.FoldersInterface;
import servisi.RetrofitClient;
import servisi.SaveSharedPreference;

public class ActivityAccount extends AppCompatActivity {

    private ArrayList<Account> folders;
    private long mInterval = 0;
    private Handler mHandler;



        public void run() {

            SharedPreferences s = SaveSharedPreference.getPreferences(getApplicationContext());

           String userName = s.getString("username", "abc");
//            SharedPreferences prefs = getSharedPreferences("UserLoged", MODE_PRIVATE);
//            String usernameByUser = prefs.getString("userNameUser", "");


                AcountService service = RetrofitClient.getClient().create(AcountService.class);
                Call<ArrayList<Account>> call = service.getAllAccount(userName);

                call.enqueue(new Callback<ArrayList<Account>>() {
                    @Override
                    public void onResponse(Call<ArrayList<Account>> call, Response<ArrayList<Account>> response) {
                        folders = response.body();
                        AccountAdapter foldersAdapter = new AccountAdapter(ActivityAccount.this, R.layout.accoutn, folders);

                        ListView listViewFolders = findViewById(R.id.list_account);
                        listViewFolders.setAdapter(foldersAdapter);

                        listViewFolders.setOnItemClickListener(new AdapterView.OnItemClickListener()
                        {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
                            {

                                Log.d("position", String.valueOf(position));

                                Log.d("sa", "asa");
                                Log.d("sa", "asa");
                                Log.d("sa", "asa");

                                Account a= new Account();
                                    a=folders.get(position);

                                Log.d("id----",  String.valueOf(a.getId()));
                                Log.d("id----",  String.valueOf(a.getPassword()));
                                Log.d("id----",  String.valueOf(a.getMessages()));
                                Log.d("id----",  String.valueOf(a.getUsername()));

                            SaveSharedPreference.setLoggedIn(getApplicationContext(), a);
                            SaveSharedPreference.setRefreshrate(getApplicationContext(), "1");
                                Intent folder = new Intent(ActivityAccount.this, EmailsActivity.class);


                                startActivity(folder);
                            }
                        });



                    }

                    @Override
                    public void onFailure(Call<ArrayList<Account>> call, Throwable t) {
                        Log.d("sa", "asa");
                        Log.d("sas", t.getMessage());
                        Log.d("sss", t.getStackTrace().toString());
                        if (t instanceof IOException) {
                            Toast.makeText(ActivityAccount.this, "this is an actual network failure :( inform the user and possibly retry", Toast.LENGTH_SHORT).show();
                            // logging probably not necessary
                        }
                        else {
                            Toast.makeText(ActivityAccount.this, "conversion issue! big problems :(", Toast.LENGTH_SHORT).show();
                            // todo log to some central bug tracking service
                        }
                        Toast.makeText(ActivityAccount.this ,"sss", Toast.LENGTH_LONG);
                    }
                });


            }







    @Override
    protected void onResume() {
        super.onResume();



        run();
        SharedPreferences s = SaveSharedPreference.getPreferences(getApplicationContext());

        String userName = s.getString("username", "abc");
        Button regis=(Button)findViewById(R.id.addAccaunt);
        regis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(ActivityAccount.this, CreateAccount.class);
                intent.putExtra("userNameUser",userName);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();

            }


        });


    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
    }
}
