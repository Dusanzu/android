package com.example.projekatandroid;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import adapteri.AttachmentForEmailAdapter;
import adapteri.EmailAdapter;
import model.Message;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import servisi.MessagesInterface;
import servisi.RetrofitClient;
import servisi.SaveSharedPreference;


public class SettingsActivity extends PreferenceActivity {
public static int vreme=500;

    private ListView listView2;
    private String username;
    private AttachmentForEmailAdapter mAdapter2;
    private ArrayList<Message> messages;
   private EmailAdapter mAdapter;

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        SharedPreferences s = SaveSharedPreference.getPreferences(getApplicationContext());

        Intent izEmailsa = getIntent();
        username = (String) izEmailsa.getExtras().getSerializable("token");
        CheckBoxPreference desc=(CheckBoxPreference) findPreference("sortiranje_opadajuce");
        desc.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if(desc.isChecked()){
                    MessagesInterface service = RetrofitClient.getClient().create(MessagesInterface.class);
                Call<ArrayList<Message>> call = service.getAllMessagesDesc(username);
                call.enqueue(new Callback<ArrayList<Message>>() {

                    @Override
                    public void onResponse(Call<ArrayList<Message>> call, Response<ArrayList<Message>> response) {
//                progressDoalog.dismiss();
                        ArrayList<Message>mes;
                        messages= response.body();
//                        messages.sort(Comparator.comparing(Message :: getDateTime).reversed());

                        ListView listView =(ListView)findViewById(R.id.emailLista);
                       EmailAdapter mAdapter=new EmailAdapter(getApplicationContext(),messages.size(),messages);
//                       listView.setAdapter(mAdapter);
                        Intent folders = new Intent(SettingsActivity.this, EmailsActivity.class);
                        startActivity(folders);

                    }

                    @Override
                    public void onFailure(Call<ArrayList<Message>> call, Throwable t) {
//                progressDoalog.dismiss();
                        Log.d("dadsda","fdsd");
                        Log.d("asssss",t.getMessage());
                        Log.d("dgdh",t.getStackTrace().toString());
                        Toast.makeText(SettingsActivity.this, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
                    }
                });
                }

                return true;
            }
        });



        CheckBoxPreference asc=(CheckBoxPreference) findPreference("sortiranje_rastuce");
        asc.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if(asc.isChecked()){
                    MessagesInterface service = RetrofitClient.getClient().create(MessagesInterface.class);
                    Call<ArrayList<Message>> call = service.getAllMessagesAsc(username);
                    call.enqueue(new Callback<ArrayList<Message>>() {

                        @Override
                        public void onResponse(Call<ArrayList<Message>> call, Response<ArrayList<Message>> response) {
//                progressDoalog.dismiss();
                            messages= response.body();
//                            messages.sort(Comparator.comparing(Message :: getDateTime));
                           ListView listView =(ListView)findViewById(R.id.emailLista);
                             mAdapter=new EmailAdapter(getApplicationContext(),messages.size(),messages);
    //                        listView.setAdapter(mAdapter);
                            Intent folders = new Intent(SettingsActivity.this, EmailsActivity.class);
                            startActivity(folders);

                        }

                        @Override
                        public void onFailure(Call<ArrayList<Message>> call, Throwable t) {
//                progressDoalog.dismiss();
                            Log.d("dadsda","fdsd");
                            Log.d("asssss",t.getMessage());
                            Log.d("dgdh",t.getStackTrace().toString());
                            Toast.makeText(SettingsActivity.this, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

                return true;
            }
        });


        CheckBoxPreference mEnable_icon = (CheckBoxPreference) findPreference("pref_sync");
        mEnable_icon.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {

            public boolean onPreferenceClick(Preference preference) {

                if(false==mEnable_icon.isChecked()){
                vreme=-1;}
                Log.d("animalsList"," animalsList    --"+vreme);


                Log.d("cech"," animalsList    --"+mEnable_icon.isChecked());


                if(true==mEnable_icon.isChecked()){


                    final ListPreference animalsList = (ListPreference) findPreference( "ListPreferencAndroid");

                    animalsList.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                        public boolean onPreferenceChange(Preference preference, Object newValue) {




                            if (animalsList.getValue().equals("1")) {
                                SaveSharedPreference.setRefreshrate(getApplicationContext(), "1");

                                vreme=6000;




                            }  else if (animalsList.getValue().equals("10")){
                                vreme=1500;
                                SaveSharedPreference.setRefreshrate(getApplicationContext(), "10");
                                Log.d("prikasvreme","animalsList"+animalsList+"vreme"+vreme+" animalsList.getValue() "+animalsList.getValue());







                            } else if (animalsList.getValue().equals("30")) {
                                vreme=2000;
                                Log.d("prikasvreme","animalsList"+animalsList+"vreme"+vreme+" animalsList.getValue() "+animalsList.getValue());
                                SaveSharedPreference.setRefreshrate(getApplicationContext(), "30");



                } else if (String.valueOf(animalsList).equals("Lista 60 min")) {
                    vreme=2000;
                    Log.d("prikasvreme","animalsList"+animalsList+"vreme"+vreme);
                                SaveSharedPreference.setRefreshrate(getApplicationContext(), "60");


                } else if (String.valueOf(animalsList).equals("-1")) {
                    vreme=-1;
                    Log.d("prikasvreme","animalsList"+animalsList+"vreme"+vreme);

                                SaveSharedPreference.setRefreshrate(getApplicationContext(), "1");


                            } else if (animalsList.getValue().equals("60")) {
                                vreme=-1;
                                Log.d("prikasvreme","animalsList"+animalsList+"vreme"+vreme+" animalsList.getValue() "+animalsList.getValue());


                                SaveSharedPreference.setRefreshrate(getApplicationContext(), "1");


                            } else {
                                Log.d("prikasvreme","animalsList"+animalsList+"vreme"+vreme+" animalsList.getValue() "+animalsList.getValue());
                                vreme=500;
                                SaveSharedPreference.setRefreshrate(getApplicationContext(), "1");
                            }

                            return true;
                        }
                    });


                }


                Log.d("prikaz"," ListPreference ++2++++"+vreme);




                return true;




            }


        });







    }



    @Override
    protected void onResume() {
        super.onResume();

    }




    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}
