package com.example.projekatandroid;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import adapteri.AttachmentForEmailAdapter;
import adapteri.EmailAdapter;
import model.Attachment;
import model.Message;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import servisi.MessagesInterface;
import servisi.RetrofitClient;
import servisi.SaveSharedPreference;

public class TagActivity2 extends AppCompatActivity {
    private Message message;
    private DrawerLayout drawer;
    private ActionBarDrawerToggle toggle;
    private ListView listView;
    private ListView listView2;
    private EmailAdapter mAdapter;
    private AttachmentForEmailAdapter mAdapter2;
    private ArrayList<Message> messages;
    private ArrayList<Attachment> attachments;
    ProgressDialog progressDoalog;
    private CheckBox ch,ch1;

    private long mInterval = 0;
    private Handler mHandler;

    private String userName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tag2);
        SharedPreferences s = SaveSharedPreference.getPreferences(getApplicationContext());

        userName = s.getString("username", "abc");

        MessagesInterface service = RetrofitClient.getClient().create(MessagesInterface.class);
        Call<ArrayList<Message>> call = service.getAllMessagesFavorte(userName);
        call.enqueue(new Callback<ArrayList<Message>>() {
            @Override
            public void onResponse(Call<ArrayList<Message>> call, Response<ArrayList<Message>> response) {
//                progressDoalog.dismiss();
                messages= response.body();
                listView =(ListView)findViewById(R.id.list_tag2);
                mAdapter=new EmailAdapter(TagActivity2.this,messages.size(),messages);
                listView.setAdapter(mAdapter);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
                {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
                    {
                        Log.d("naslov13",String.valueOf(position));
                        Intent setings = new Intent(TagActivity2.this, EmailActivity.class);
                        setings.putExtra("EMAIL", messages.get(position));
                        startActivity(setings);
                    }
                });
            }
            @Override
            public void onFailure(Call<ArrayList<Message>> call, Throwable t) {
//                progressDoalog.dismiss();
                Toast.makeText(TagActivity2.this, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
            }
        });

    }
}
