package com.example.projekatandroid;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;

import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Toast;


import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import adapteri.DraftAdapter;
import etc.FileUtils;
import etc.Utility;
import model.Attachment;
import model.Contact;
import model.Message;
import model.Tag;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import servisi.MessagesInterface;
import servisi.RetrofitClient;
import servisi.SaveSharedPreference;


public class CreateEmailActivity extends AppCompatActivity {
    Date date;

    private Message message = new Message();


    private MultiAutoCompleteTextView multiAuto;
    private EditText  multiTo, naslov, poruka;
    private DraftAdapter adapter;
    private ListView listView;
    private List<String>lista;
    private ArrayList<String>listaEmailsa;
    private String userName;
    private MessagesInterface messagesInterface = RetrofitClient.getClient().create(MessagesInterface.class);
    private Button buttonAddAttachment;
    private static final int READ_REQUEST_CODE = 42;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences s = SaveSharedPreference.getPreferences(getApplicationContext());

        userName = s.getString("username", "abc");

        setContentView(R.layout.activity_create_email);

        Toolbar toolbar = findViewById(R.id.toolbarCreateEmail);
        setSupportActionBar(toolbar);
        toolbar.inflateMenu(R.menu.create_emails);

    }

    @Override
    protected void onResume() {
        super.onResume();

        buttonAddAttachment = findViewById(R.id.buttonAddAttachment);

        Intent izEmailsa = getIntent();
        Message folder = (Message) izEmailsa.getExtras().getSerializable("Email");
        //todo probaj bez ovoga
//        Intent izEmailsa1 = getIntent();
        //todo ajde
        Message folder1 = (Message) izEmailsa.getExtras().getSerializable("Email1");

        Intent izFolder = getIntent();
        Message folderDraft = (Message) izFolder.getExtras().getSerializable("DRAFT_EMAIL");
        //todo probaj bez ovoga
//        Intent izEmailsa2 = getIntent();
        //todo ajde
        String string1 = (String)izEmailsa.getExtras().getSerializable("EMAIL3");
        if(folder != null) {
            LocalDate localDate;
            date = new Date();


            naslov = findViewById(R.id.subject);
            poruka = findViewById(R.id.content);
            multiTo=findViewById(R.id.multiTo);
            poruka.setText(folder.getContent());
            naslov.setText(folder.getSubject());
            multiTo.setText(folder.getFrom());


        }

        else if(folderDraft != null) {
            LocalDate localDate;
            date = new Date();

            multiTo=findViewById(R.id.multiTo);
            multiTo.setText(folderDraft.getTo().get(0));
            naslov = findViewById(R.id.subject);
            poruka = findViewById(R.id.content);
            poruka.setText(folderDraft.getSubject());
            naslov.setText(folderDraft.getContent());

//            multiAuto.setText(folderDraft.getFrom());
        }
        else if(folder1 != null) {
            LocalDate localDate;
            date = new Date();

            multiTo=findViewById(R.id.multiTo);

            naslov = findViewById(R.id.subject);
            poruka = findViewById(R.id.content);
            poruka.setText(folder1.getContent());
            naslov.setText(folder1.getSubject());
//            multiAuto.setText(folder1.getFrom());

        }
        else if(string1 != null) {
            naslov = findViewById(R.id.subject);
            naslov.getText();
            poruka = findViewById(R.id.content);
            poruka.getText();
            multiTo=findViewById(R.id.multiTo);
            multiTo.getText();
        }
        //za dodavanje attachmenta
        buttonAddAttachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performFileSearch();

            }
        });
        //
    }
    public ArrayList<String> getEmails(){
        ArrayList<String>lista=new ArrayList<>();
        if(!(multiTo.getText().toString().endsWith("@gmail.com"))) {
            Toast.makeText(CreateEmailActivity.this, "pogresan format emaila", Toast.LENGTH_LONG).show();
            return null;
        }
        lista.add(multiTo.getText().toString());
        return lista;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.create_emails,menu);
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id=item.getItemId();
        if(id==R.id.btnSendEmail){

            SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date(System.currentTimeMillis());


            naslov=findViewById(R.id.subject);
            poruka=findViewById(R.id.content);
            ArrayList<String>listaZaSlanje= getEmails();
            List<String>prazna=new ArrayList<>();
            List<Attachment>prazna1=new ArrayList<>();
            List<Tag>prazna2=new ArrayList<>();

            Contact account = (Contact) getIntent().getSerializableExtra("PROFILE");
            Message m=new Message(userName,listaZaSlanje,prazna, prazna,date,naslov.getText().toString(),poruka.getText().toString(),prazna1,prazna2);
            message.setFrom(userName);
            message.setTo(listaZaSlanje);
            message.setCc(prazna);
            message.setBcc(prazna);
            message.setDateTime(date);
            message.setSubject(naslov.getText().toString());
            message.setContent(poruka.getText().toString());
            message.setTags(prazna2);

            MessagesInterface service = RetrofitClient.getClient().create(MessagesInterface.class);


            Call<Message> call = service.send(message, userName);

            call.enqueue(new Callback<Message>() {
                @Override
                public void onResponse(Call<Message> call, Response<Message> response) {
                    Intent folders = new Intent(CreateEmailActivity.this, EmailsActivity.class);
                    startActivity(folders);

                }

                @Override
                public void onFailure(Call<Message> call, Throwable t) {

                }
            });



            Toast.makeText(CreateEmailActivity.this, "Kliknuli ste send dugme EmailActivity", Toast.LENGTH_LONG).show();
        }


        if(id==R.id.btnback){
            addToDrafts();

        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }



    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {

        addToDrafts();



    }

    public void addToDrafts(){
        List<String>li=getEmails();
        Toast.makeText(CreateEmailActivity.this, "Usao u add to draft", Toast.LENGTH_LONG);
        if(naslov.getText() != null || !naslov.getText().equals("") ||
                poruka.getText() != null || !poruka.getText().equals("") ||
                !li.isEmpty()) {
            Message message = new Message();
            message.setBcc(new ArrayList<>());
            message.setCc(new ArrayList<>());

            message.setTo(li);
            message.setFrom(userName);
            message.setSubject(naslov.getText().toString());
            message.setContent(poruka.getText().toString());
            message.setProcitano(true);
            message.setAttachments(new ArrayList<>());
            SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date(System.currentTimeMillis());
            message.setDateTime(date);
            ArrayList<Message> lista2 = new ArrayList<Message>();
            lista2.add(message);

            Call<Message> deleteCall = messagesInterface.addToDraft(message, userName);

            deleteCall.enqueue(new Callback<Message>() {
                @Override
                public void onResponse(Call<Message> call, Response<Message> response) {


                    Toast.makeText(CreateEmailActivity.this, "Poruka dodata u draft", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(CreateEmailActivity.this, EmailsActivity.class);
                    startActivity(intent);
                    finish();
                }

                @Override
                public void onFailure(Call<Message> call, Throwable t) {
                    Toast.makeText(CreateEmailActivity.this, "Poruka bezuspesno dodata u draft", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(CreateEmailActivity.this, EmailsActivity.class);
                    startActivity(intent);
                    finish();
                }
            });
        } else {
            Toast.makeText(CreateEmailActivity.this, "Nije dodato u draft", Toast.LENGTH_LONG);
            Intent intent = new Intent(CreateEmailActivity.this, EmailsActivity.class);
            startActivity(intent);
            finish();
        }


    }

    private String getFileExtension(File file) {
        String name = file.getName();
        int lastIndexOf = name.lastIndexOf(".");
        if (lastIndexOf == -1) {
            return ""; // empty extension
        }
        return name.substring(lastIndexOf);
    }

    public void performFileSearch() {

        // ACTION_OPEN_DOCUMENT is the intent to choose a file via the system's file
        // browser.
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);

        // Filter to only show results that can be "opened", such as a
        // file (as opposed to a list of contacts or timezones)
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        // Filter to show only images, using the image MIME data type.
        // If one wanted to search for ogg vorbis files, the type would be "audio/ogg".
        // To search for all documents available via installed storage providers,
        // it would be "*/*".
        intent.setType("*/*");

        startActivityForResult(intent, READ_REQUEST_CODE);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode,
                                 Intent resultData) {

        // The ACTION_OPEN_DOCUMENT intent was sent with the request code
        // READ_REQUEST_CODE. If the request code seen here doesn't match, it's the
        // response to some other intent, and the code below shouldn't run at all.

        if (requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            // The document selected by the user won't be returned in the intent.
            // Instead, a URI to that document will be contained in the return intent
            // provided to this method as a parameter.
            // Pull that URI using resultData.getData().
            Uri uri = null;
            if (resultData != null) {
                uri = resultData.getData();
                Log.i("perica", "Uri: " + uri.toString());
                Toast.makeText(CreateEmailActivity.this, uri.toString(), Toast.LENGTH_LONG).show();
//                showImage(uri);
                uploadFile(uri);
            }
        }
    }
    private void uploadFile(Uri fileUri) {


        // https://github.com/iPaulPro/aFileChooser/blob/master/aFileChooser/src/com/ipaulpro/afilechooser/utils/FileUtils.java
        // use the FileUtils to get the actual file by uri
        File file = FileUtils.getFile(this, fileUri);



        int size = (int) file.length();
//        byte[] bytes = new byte[size];
        byte[] bytes = null;
        try {
            bytes = org.apache.commons.io.FileUtils.readFileToByteArray(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String enkodovano = Base64.encodeToString(bytes, Base64.DEFAULT);

        Attachment attachment = new Attachment();
        attachment.setData(enkodovano);
        attachment.setName(file.getName());
        attachment.setType(getFileExtension(file));

        if (message.getAttachments() == null)
            message.setAttachments(new ArrayList<>());

        message.getAttachments().add(attachment);

        //todo obrisi
//        // create RequestBody instance from file
//        RequestBody requestFile =
//                RequestBody.create(
//                        MediaType.parse(getContentResolver().getType(fileUri)),
//                        file
//                );

        // MultipartBody.Part is used to send also the actual file name
//        MultipartBody.Part body =
//                MultipartBody.Part.createFormData("picture", file.getName(), requestFile);

        // add another part within the multipart request
//        String descriptionString = "hello, this is description speaking";
//        RequestBody description =
//                RequestBody.create(
//                        okhttp3.MultipartBody.FORM, descriptionString);

        // finally, execute the request

//        Call<ResponseBody> call = messagesInterface.upload("ajde", body);
//        call.enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call,
//                                   Response<ResponseBody> response) {
//                Log.v("Upload", "success");
//                Toast.makeText(CreateEmailActivity.this ,"Upload prosao", Toast.LENGTH_LONG).show();
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                Log.e("Upload error:", t.getMessage());
//                Toast.makeText(CreateEmailActivity.this ,"Upload nije prosao", Toast.LENGTH_LONG).show();
//            }
//        });
        //todo obrisi
    }

}
