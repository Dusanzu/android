package com.example.projekatandroid;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Date;

import model.Base64;
import model.Contact;
import model.Photo;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import servisi.ContactsInterface;
import servisi.RetrofitClient;
import servisi.SaveSharedPreference;

public class CreateContactActivity extends AppCompatActivity {
    EditText Name;
    EditText LastName;
    EditText Emails;
    EditText editDisplay;

    private DrawerLayout drawer;
    private ActionBarDrawerToggle toggle;
    private String userName;
    private static String formatt="";
   ImageView img;
   Photo ph;
    Base64 base64;
    private static  final  int IMAGE_PICK_CODE=1000;
    private static  final  int PERMISION_CODE=1001;
    private static int RESULT_LOAD_IMAGE = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_contact);

        Toolbar toolbar = findViewById(R.id.toolbarCreateContact);
        setSupportActionBar(toolbar);
        toolbar.inflateMenu(R.menu.create_contact_toolbar);

         img = (ImageView) findViewById(R.id.imageView5);
        Button buttonLoadImage = (Button) findViewById(R.id.but);
        buttonLoadImage.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                    if(checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                        ==PackageManager.PERMISSION_DENIED){
                        String[] permission={Manifest.permission.READ_EXTERNAL_STORAGE};
                        requestPermissions(permission,PERMISION_CODE);

                    }else {
                        pickFromGalery();
                    }
                }
                else {
                    pickFromGalery();
                }

            }
        });
    }
    private void pickFromGalery(){
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, IMAGE_PICK_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case PERMISION_CODE:{
                if(grantResults.length>0 && grantResults[0]== PackageManager.PERMISSION_GRANTED){
                    pickFromGalery();
                }else {
                    Toast.makeText(CreateContactActivity.this, "You haven't picked Image",Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == IMAGE_PICK_CODE) {
            img.setImageURI(data.getData());
            ph=new Photo();
            ph.setPath(data.getData().toString());

        }



    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.create_contact_toolbar,menu);
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();

        SharedPreferences s = SaveSharedPreference.getPreferences(getApplicationContext());

        userName = s.getString("username", "abc");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }



    @Override
    protected void onResume() {
       super.onResume();

    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id=item.getItemId();
        if(id==R.id.btnBackFromCreateContact){
            Toast.makeText(CreateContactActivity.this, "nazad", Toast.LENGTH_LONG).show();

            Intent profile = new Intent(CreateContactActivity.this, ContactsActivity.class);
            startActivity(profile);
        }

        if(id==R.id.btnSaveCreateContact){


            Log.d("CreateContactActivity","1");


            Name = (EditText) findViewById(R.id.IdcreateContactName);
            LastName = (EditText) findViewById(R.id.IdcreateContactlastName);
            Emails = (EditText) findViewById(R.id.IdcreateContactEmails);
            editDisplay= (EditText) findViewById(R.id.editDisplayCreate);
            img = (ImageView) findViewById(R.id.imageView5);


//            String url="https://static.wixstatic.com/media/200fe1_06ce139451b6433d95342587d4542c01.png";
//            byte[] basic = url.getBytes();
//            String basic64Str = "";
//            try {
//                basic64Str = base64.encrypte(basic);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
            String Name1 = Name.getText().toString();
            String LastName1 = LastName.getText().toString();
            String Emails1 = Emails.getText().toString();
            String Display = editDisplay.getText().toString();
            Log.d("CreateContactActivity","2" );

            int i=0;
            if(Display.equals("")){
                Toast.makeText(CreateContactActivity.this, "Display Name", Toast.LENGTH_SHORT).show();
            i=-1;
            }
            if(Name1.equals("")){
                Toast.makeText(CreateContactActivity.this, "Input Name", Toast.LENGTH_SHORT).show();
                i=-1;
            }
            if(formatt.equals("")){
                Toast.makeText(CreateContactActivity.this, "Input formatt", Toast.LENGTH_SHORT).show();
                i=-1;
            }
            if(LastName1.equals("")){
                Toast.makeText(CreateContactActivity.this, "Input Last Name", Toast.LENGTH_SHORT).show();
                i=-1;
            }

            if(LastName1.equals("")){
                Toast.makeText(CreateContactActivity.this, "Input Last Name", Toast.LENGTH_SHORT).show();
                i=-1;
            }
            if(Emails1.equals("")){
                Toast.makeText(CreateContactActivity.this, "Input emails", Toast.LENGTH_SHORT).show();
                i=-1;
            }
            String em = Emails.getText().toString();
            if (!(em.contains("@gmail.com"))) {
                Toast.makeText(CreateContactActivity.this, "emails is  wrong", Toast.LENGTH_SHORT).show();
                i = -1;
            }


            if(i==0){





                ContactsInterface service = RetrofitClient.getClient().create(ContactsInterface.class);

                Contact contact = new Contact(hashCode(), Name1, LastName1, Display,
                        Emails1, Contact.Format.valueOf(formatt), ph);

                Call<Void> call = service.addContact(contact, userName);
                call.enqueue(new Callback<Void>() {

                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
//                        img = (ImageView) findViewById(R.id.imageView5);
//                        Picasso.get().load("https://i.imgur.com/tGbaZCY.jpg").into(img);
//
                        Toast.makeText(CreateContactActivity.this, "dodato", Toast.LENGTH_LONG).show();
                        Log.d("NOvi kontakt",contact.getPhoto().getPath());
                        Intent i=new Intent(CreateContactActivity.this, ContactsActivity.class);
                        startActivity(i);
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        Toast.makeText(CreateContactActivity.this, "Something went wrong...Please try later!", Toast.LENGTH_SHORT);
                    }
                });
            }






//            Intent profile = new Intent(CreateContactActivity.this, EmailsActivity.class);
//            startActivity(profile);
        }



        return super.onOptionsItemSelected(item);
    }

    public void onCreateButtonClicked(View view){
        boolean checked = ((RadioButton) view).isChecked();

        switch (view.getId()){
            case R.id.radio_htmlCreate:
                if (checked)
                    formatt="HTML";
                break;
            case R.id.radio_plainCreate:
                if (checked)
                    formatt="PLAIN";
                break;

        }
    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
