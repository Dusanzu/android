package com.example.projekatandroid;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import java.util.ArrayList;

import model.Folder;
import model.Rule;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import servisi.FoldersInterface;
import servisi.RetrofitClient;
import servisi.SaveSharedPreference;

public class CreateFolderActivity extends AppCompatActivity {

    private Folder folder = new Folder(100, "folder100", new ArrayList<Folder>(), new Rule
            (1, Rule.Condition.FROM, Rule.Operation.MOVE), new ArrayList<>() );

    private boolean izmena;
    private EditText editTextFolderName;
    private EditText subjectFolder;
    private String userName;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_folder);
        Toolbar toolbar = findViewById(R.id.toolbarCreateFolder);
        setSupportActionBar(toolbar);
        editTextFolderName = findViewById(R.id.editTextFolderName);
        subjectFolder = findViewById(R.id.subjectFolder);

        izmena = getIntent().getBooleanExtra("IZMENA", false);
        if (izmena){
            subjectFolder.setVisibility(View.INVISIBLE);
            getSupportActionBar().setTitle("Edit folder");
            folder = (Folder) getIntent().getSerializableExtra("FOLDER");
            editTextFolderName.setText(folder.getName());
        } else{
            getSupportActionBar().setTitle("Create folder");
        }
        RadioButton radioButtonOperation;
        switch (folder.getRule().getOperation()){
            case MOVE:
                radioButtonOperation = findViewById(R.id.radio_MOVE);
                radioButtonOperation.setChecked(true);
                break;
            case COPY:
                radioButtonOperation = findViewById(R.id.radio_COPY);
                radioButtonOperation.setChecked(true);
                break;
            case DELETE:
                radioButtonOperation = findViewById(R.id.radio_DELETE);
                radioButtonOperation.setChecked(true);
                break;
        }

        RadioButton radioButtonCondition;

        switch (folder.getRule().getCondition()){
            case TO:
                radioButtonCondition = findViewById(R.id.radio_TO);
                radioButtonCondition.setChecked(true);
                break;
            case FROM:
                radioButtonCondition = findViewById(R.id.radio_FROM);
                radioButtonCondition.setChecked(true);
                break;
            case SUBJECT:
                radioButtonCondition = findViewById(R.id.radio_SUBJECT);
                radioButtonCondition.setChecked(true);
                break;
            case CC:
                radioButtonCondition = findViewById(R.id.radio_CC);
                radioButtonCondition.setChecked(true);
                break;
        }



        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }
    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {

        super.onResume();

        SharedPreferences s = SaveSharedPreference.getPreferences(getApplicationContext());

        userName = s.getString("username", "abc");


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.create_folder_toolbar,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id=item.getItemId();
        if(id==R.id.btnCancelFromCreateFolder){
            Toast.makeText(CreateFolderActivity.this, "Obrisano", Toast.LENGTH_LONG).show();
        }
        if (id == R.id.btnSaveFromCreateFolder){
            Toast.makeText(CreateFolderActivity.this, "Sacuvano", Toast.LENGTH_LONG).show();


            FoldersInterface foldersInterface = RetrofitClient.getClient().create(FoldersInterface.class);
            EditText editTextFolderName = findViewById(R.id.editTextFolderName);


            folder.setName(editTextFolderName.getText().toString());

            if (izmena && !editTextFolderName.getText().toString().equals("")) {
                Call<Folder> updateFolder = foldersInterface.updateFolders(folder, folder.getId());
                updateFolder.enqueue(new Callback<Folder>() {
                    @Override
                    public void onResponse(Call<Folder> call, Response<Folder> response) {
                        folder = response.body();
                        Intent profile = new Intent( CreateFolderActivity.this, FoldersActivity.class);
                        startActivity(profile);
                        Toast.makeText(CreateFolderActivity.this, "Izmenjen folder", Toast.LENGTH_SHORT);
                        finish();
                    }

                    @Override
                    public void onFailure(Call<Folder> call, Throwable t) {
                        Toast.makeText(CreateFolderActivity.this, "Izmena nije uspela", Toast.LENGTH_SHORT);
                        finish();
                    }
                });
            } else {
                if (!editTextFolderName.getText().toString().equals("")){
                    String prebaciSveEmailoveOd = subjectFolder.getText().toString();
                    if (prebaciSveEmailoveOd.equals(""))
                        prebaciSveEmailoveOd = "q";
                    Call<Void> addFolder = foldersInterface.addNewFolder(folder, userName, prebaciSveEmailoveOd);

                    addFolder.enqueue(new Callback<Void>() {
                        @Override
                        public void onResponse(Call<Void> call, Response<Void> response) {
                            Toast.makeText(CreateFolderActivity.this, "Uspesno dodat folder", Toast.LENGTH_LONG);

                            finish();
                        }

                        @Override
                        public void onFailure(Call<Void> call, Throwable t) {
                            Toast.makeText(CreateFolderActivity.this, "Bezusepsno dodat folder", Toast.LENGTH_LONG);
                            finish();
                        }
                    });
                }

            }
        }
        if (id == android.R.id.home ){
            finish();
        }


        return super.onOptionsItemSelected(item);
    }

    public void onConditionRadioButtonClicked(View view){
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radio_TO:
                if (checked)
                    folder.getRule().setCondition(Rule.Condition.TO);
                    break;
            case R.id.radio_FROM:
                if(checked)
                    folder.getRule().setCondition(Rule.Condition.FROM);
                    break;
            case R.id.radio_CC:
                if (checked)
                    folder.getRule().setCondition(Rule.Condition.CC);
                    break;
            case R.id.radio_SUBJECT:
                if (checked)
                    folder.getRule().setCondition(Rule.Condition.SUBJECT);
                    break;
        }
    }

    public void onOperationRadioButtonClicked(View view){
        boolean checked = ((RadioButton) view).isChecked();

        switch (view.getId()){
            case R.id.radio_MOVE:
                if (checked)
                    folder.getRule().setOperation(Rule.Operation.MOVE);
                    break;
            case R.id.radio_COPY:
                if (checked)
                    folder.getRule().setOperation(Rule.Operation.COPY);
                    break;
            case R.id.radio_DELETE:
                if (checked)
                    folder.getRule().setOperation(Rule.Operation.DELETE);
                    break;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
