package com.example.projekatandroid;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import adapteri.ContactAdapter;
import model.Contact;
import model.Message;
import model.Photo;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import servisi.ContactsInterface;
import servisi.RetrofitClient;
import servisi.SaveSharedPreference;

public class ContactsActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout drawer;
    private ActionBarDrawerToggle toggle;
    private List<Contact> contacts;

    private String userName;

    private long mInterval;
    private Handler mHandler;




    @Override
    protected void onStart() {
        super.onStart();
        String[]  IMENA={"pera","petar","petko"};
        SharedPreferences s = SaveSharedPreference.getPreferences(getApplicationContext());

        userName = s.getString("username", "abc");
        int[] SLIKE={R.drawable.ee,R.drawable.qq,R.drawable.ww};

    }

    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            try {
                SharedPreferences pref = SaveSharedPreference.getPreferences(getApplicationContext());
                String syncTimeStr = pref.getString("refresh_rate", "1");
                Log.d("SYNC TIME", syncTimeStr);
                mInterval = TimeUnit.MINUTES.toMillis(Integer.parseInt(syncTimeStr));

                Toast toast = Toast.makeText(getApplicationContext(), "Syncing", Toast.LENGTH_SHORT);
                toast.show();





                ContactsInterface service = RetrofitClient.getClient().create(ContactsInterface.class);
                Call<List<Contact>> call = service.getAllContacts(userName);
                call.enqueue(new Callback<List<Contact>>() {
                    @Override
                    public void onResponse(Call<List<Contact>> call, Response<List<Contact>> response) {

                        contacts=response.body();

                        ContactAdapter contactAdapter = new ContactAdapter(ContactsActivity.this, R.layout.contactcustom, contacts);

                        ListView listviev=(ListView)findViewById(R.id.listVievContacts);


                        listviev.setAdapter(contactAdapter);

                        listviev.setOnItemClickListener(new AdapterView.OnItemClickListener()
                        {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View  view, int position, long id)
                            {
                                Intent contact = new Intent(ContactsActivity.this, ContactActivity.class);
                                contact.putExtra("CONTACT", contacts.get(position));
                                startActivity(contact);
                            }


                        });
                    }

                    @Override
                    public void onFailure(Call<List<Contact>> call, Throwable t) {
                        Log.d("sa", "asa");
                        Log.d("sas", t.getMessage());
                        Log.d("sss", t.getStackTrace().toString());
                        if (t instanceof IOException) {
                            Toast.makeText(ContactsActivity.this, "this is an actual network failure :( inform the user and possibly retry", Toast.LENGTH_SHORT).show();
                            // logging probably not necessary
                        }
                        else {
                            Toast.makeText(ContactsActivity.this, "conversion issue! big problems :(", Toast.LENGTH_SHORT).show();
                            // todo log to some central bug tracking service
                        }
                    }
                });


            }finally {
                mHandler.postDelayed(mStatusChecker, mInterval);
            }

        }
    };
    void startRepeatingTask() {
        mStatusChecker.run();
    }

    void stopRepeatingTask(){
        mHandler.removeCallbacks(mStatusChecker);
    }



    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();

        startRepeatingTask();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        mHandler = new Handler();





        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);

        Toolbar tool=findViewById(R.id.toolbarContacts);
        setSupportActionBar(tool);


        tool.inflateMenu(R.menu.contacts_toolbar);


        drawer=findViewById(R.id.drawer_contacts);

        toggle=new ActionBarDrawerToggle(this,drawer,tool,R.string.drawable_layout_otvori,R.string.drawable_layout_zatvori);

        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navView = findViewById(R.id.nav_contacts);
        navView.setNavigationItemSelectedListener(this);

        View header=navView.getHeaderView(0);
        header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent setings = new Intent(ContactsActivity.this, ProfileActivity.class);
                startActivity(setings);
            }
        });


        FloatingActionButton fab = findViewById(R.id.fabContacts);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent setings = new Intent(ContactsActivity.this, CreateContactActivity.class);
                startActivity(setings);
                Toast.makeText(ContactsActivity.this, "Kliknuli ste FAB dugme ContactsActivity", Toast.LENGTH_LONG).show();
            }
        });

    }

    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        switch (menuItem.getItemId()) {

            case R.id.profile: {
                Intent profile = new Intent(ContactsActivity.this,ContactsActivity.class);
                startActivity(profile);
                break;
            }
            case R.id.folders: {
                Intent folders = new Intent(ContactsActivity.this, FoldersActivity.class);
                startActivity(folders);
                break;
            }
            case R.id.settings: {
                Intent seti = new Intent(ContactsActivity.this, SettingsActivity.class);
                startActivity(seti);
                break;
            }
            case R.id.fav: {
                Intent folders = new Intent(ContactsActivity.this, TagActivity2.class);
                startActivity(folders);
                break;
            }
            case R.id.lik: {
                Intent folders = new Intent(ContactsActivity.this, TagActivity.class);
                startActivity(folders);
                break;
            }
            case R.id.emails: {
                Intent emails = new Intent(ContactsActivity.this, EmailsActivity.class);
                startActivity(emails);
                break;
            }
            case R.id.odjava: {
                SaveSharedPreference.getLoggedStatus(getApplicationContext());
                break;
            }

        }
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id=item.getItemId();
        if(id==R.id.btnBackContacts){
            Toast.makeText(ContactsActivity.this, "nazad", Toast.LENGTH_LONG).show();
            Intent profile = new Intent(ContactsActivity.this, EmailsActivity.class);
            startActivity(profile);
        }


        if(id==R.id.btnAddContacts){
            Toast.makeText(ContactsActivity.this, "dodato", Toast.LENGTH_LONG).show();
            Intent profile = new Intent(ContactsActivity.this, CreateContactActivity.class);
            startActivity(profile);
        }
        return super.onOptionsItemSelected(item);
    }






    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.contacts_toolbar,menu);
        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopRepeatingTask();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
