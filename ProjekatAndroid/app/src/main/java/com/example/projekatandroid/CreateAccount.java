package com.example.projekatandroid;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import model.Account;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import servisi.AcountService;
import servisi.RetrofitClient;
import servisi.SaveSharedPreference;

public class CreateAccount extends AppCompatActivity {



    EditText Name;
    EditText LastName;
    EditText password;
    EditText confirmPassword;
    EditText userName;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);






//        FloatingActionButton fab = findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
    }
    @Override
    protected void onResume() {
        super.onResume();


        Intent izEmailsa = getIntent();
        String usernameByUser = (String) izEmailsa.getExtras().getSerializable("userNameUser");

        Button regis=(Button)findViewById(R.id.CreateAccauntNazad);
        regis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(CreateAccount.this, ActivityAccount.class);
                intent.putExtra("userNameUser",usernameByUser);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();

            }


        });












        Button btnRegistracija=(Button)findViewById(R.id.CreateAccauntDugme);


        btnRegistracija.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Name = (EditText) findViewById(R.id.editEmailCreateAccount);

                password = (EditText) findViewById(R.id.editpassvordCreateAccount);
                confirmPassword= (EditText) findViewById(R.id.editconfirmpassvordCreateAccount);



                String name = Name.getText().toString();

                String password1 = password.getText().toString();
                String confirmPassword1 = confirmPassword.getText().toString();



                int i=0;
                if(name.equals("")){
                    Toast.makeText(CreateAccount.this, "name empty", Toast.LENGTH_SHORT).show();
                    i=-1;
                }

                if(password1.equals("")){
                    Toast.makeText(CreateAccount.this, "password is empty", Toast.LENGTH_SHORT).show();
                    i=-1;
                }
                if(confirmPassword1.equals("")){
                    Toast.makeText(CreateAccount.this, "password is empty", Toast.LENGTH_SHORT).show();
                    i=-1;
                }


                if(password1.equals(confirmPassword1)==false){
                    Toast.makeText(CreateAccount.this, "passvorn error", Toast.LENGTH_SHORT).show();
                    i=-1;
                }

                SharedPreferences s = SaveSharedPreference.getPreferences(getApplicationContext());

                String userName = s.getString("username", "abc");


                if(i==0) {
                    Log.d("Retrofit","Retrofitstigao");


                    Account accaunt =new Account();
                    accaunt.setPassword(confirmPassword1);
                    accaunt.setUsername(name);
                    Retrofit retrofit = RetrofitClient.getClient();
                    AcountService services = retrofit.create(AcountService.class);


                    Call<Void> call = services.addAccaunt(accaunt,userName);

                    Log.d("Retrofit1","novo1");
                    call.enqueue(new Callback<Void>() {


                        @Override
                        public void onResponse(Call<Void> call, Response<Void> response) {
                            if (response.isSuccessful()) {
                                Log.d("Retrofit1","novo2");
                                Intent intent = new Intent(CreateAccount.this, ActivityAccount.class);

                                startActivity(intent);
                                finish();

                            }
                            Toast.makeText(CreateAccount.this, "The username or password is incorrect", Toast.LENGTH_SHORT).show();

                        }


                        @Override
                        public void onFailure(Call call, Throwable t) {
                            Toast.makeText(CreateAccount.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                            Log.d("dadsda", "fdsd");
                            Log.d("sdfdsfsd", t.getMessage());
                            Log.d("sdfdsfsd", t.getStackTrace().toString());

                        }
                    });

                }

            }
        });
    }

}


