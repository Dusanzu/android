package com.example.projekatandroid;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

//import com.squareup.picasso.Picasso;

import com.squareup.picasso.Picasso;

import model.Base64;
import model.Contact;
import model.Message;
import model.Rule;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import servisi.ContactsInterface;
import servisi.MessagesInterface;
import servisi.RetrofitClient;
import servisi.SaveSharedPreference;

import static model.Contact.Format.HTML;
import static model.Contact.Format.PLAIN;

public class ContactActivity extends AppCompatActivity {
    EditText Name;
    EditText LastName;
    EditText Emails;
    EditText Display;
    private static String formatt;
    ImageView img;
   Base64 base64;
    private DrawerLayout drawer;
    private ActionBarDrawerToggle toggle;
    Contact contact;
    RadioButton radioButtonOperation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);


        Toolbar tool = findViewById(R.id.toolbarContact1);
        setSupportActionBar(tool);
        tool.inflateMenu(R.menu.contact_tollbar);



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.contact_tollbar, menu);

        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        Contact contact = (Contact) getIntent().getSerializableExtra("CONTACT");
         Name = findViewById(R.id.editText5);

        Name.setText(contact.getFirst());

       LastName = findViewById(R.id.editText6);

        LastName.setText(contact.getLast());

        Emails = findViewById(R.id.editText7);

        Emails.setText(contact.getEmail());

        Display = findViewById(R.id.editDisplay);

        Display.setText(contact.getDisplay());



        switch (contact.getFormat()){
            case PLAIN:
                radioButtonOperation = findViewById(R.id.radio_html);
                formatt="PLAIN";
                radioButtonOperation.setChecked(true);

                break;
            case HTML:
                radioButtonOperation = findViewById(R.id.radio_plain);
                    formatt="HTML";
                radioButtonOperation.setChecked(true);
                break;

        }

    }






    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.btnBackContact) {
            Toast.makeText(ContactActivity.this, "nazad", Toast.LENGTH_LONG).show();

            Intent profile = new Intent(ContactActivity.this, ContactsActivity.class);
            startActivity(profile);
        }


        if(id==R.id.btnAddContact33) {
            Intent profile = new Intent(ContactActivity.this, CreateContactActivity.class);
            startActivity(profile);
//            Name = (EditText) findViewById(R.id.editText5);
//            LastName = (EditText) findViewById(R.id.editText6);
//            Emails = (EditText) findViewById(R.id.editText7);
//            img = (ImageView) findViewById(R.id.imageView5);
//            Picasso.get().load("https://i.imgur.com/tGbaZCY.jpg").into(img);
//            byte[] basic = img.toString().getBytes();
//            String basic64Str = "";
//            try {
//                basic64Str = base64.encrypte(basic);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//

        }

        if(id==R.id.btnupdateContact){
            Intent izEmailsa1 = getIntent();
           Contact folder1 = (Contact) izEmailsa1.getExtras().getSerializable("CONTACT");

            ImageView im=(ImageView) findViewById(R.id.imageView5);
//            Picasso.get().load(folder1.getPhoto().getPath()).into(im);
            String Name1 = Name.getText().toString();
            String LastName1 = LastName.getText().toString();
            String Emails1 = Emails.getText().toString();

            String Display1  = Display.getText().toString();
             contact=new Contact(folder1.getId(),Name1,LastName1,Display1,Emails1, Contact.Format.valueOf(formatt),folder1.getPhoto());





            ContactsInterface service = RetrofitClient.getClient().create(ContactsInterface.class);

            SharedPreferences s = SaveSharedPreference.getPreferences(getApplicationContext());

           String userName = s.getString("username", "abc");

            Call<Contact> call = service.addUpdateContact(contact,folder1.getId(),userName);
            call.enqueue(new Callback<Contact>() {

                @Override
                public void onResponse(Call<Contact> call, Response<Contact> response) {
                    contact=response.body();
                    Toast.makeText(ContactActivity.this, "izmenjeno", Toast.LENGTH_LONG).show();
//                    Toast.makeText(ContactActivity.this, Name1, Toast.LENGTH_LONG).show();
//                    Toast.makeText(ContactActivity.this, LastName1, Toast.LENGTH_LONG).show();
                    Intent i = new Intent(ContactActivity.this, ContactsActivity.class);
                    startActivity(i);
                }

                @Override
                public void onFailure(Call<Contact> call, Throwable t) {
                    Toast.makeText(ContactActivity.this, "Something went wrong...Please try later!", Toast.LENGTH_SHORT);
                }
            });





        }

        if(id==R.id.btndeleteContact){
          Intent izEmailsa1 = getIntent();
           Contact folder1 = (Contact) izEmailsa1.getExtras().getSerializable("CONTACT");
            ContactsInterface service = RetrofitClient.getClient().create(ContactsInterface.class);
            Call<Void> call = service.getDeleteContact(folder1.getId());
            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    Intent i=new Intent(ContactActivity.this, ContactsActivity.class);
                    startActivity(i);
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    Toast.makeText(ContactActivity.this, "Something went wrong...Please try later!", Toast.LENGTH_SHORT);
                }
            });

//            Toast.makeText(EmailActivity.this, "Kliknuli ste delete dugme EmailActivity", Toast.LENGTH_LONG).show();
        }

        return super.onOptionsItemSelected(item);
    }



    public void onRadioButtonClicked(View view){
        boolean checked = ((RadioButton) view).isChecked();

        switch (view.getId()){
            case R.id.radio_html:
                if (checked)
                    formatt="PLAIN";
                break;
            case R.id.radio_plain:
                if (checked)
                    formatt="HTML";
                break;

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
