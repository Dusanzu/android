package com.example.projekatandroid;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import adapteri.FoldersAdapter;
import model.Folder;
import model.Message;
import model.Rule;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import servisi.FoldersInterface;
import servisi.RetrofitClient;
import servisi.SaveSharedPreference;

public class FoldersActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout drawer;
    private ActionBarDrawerToggle toggle;
    private ArrayList<Folder> folders;
    private NavigationView navView;
    private long mInterval = 0;
    private Handler mHandler;
    private String userName;

    @Override
    protected void onStart() {
        super.onStart();
        SharedPreferences s = SaveSharedPreference.getPreferences(getApplicationContext());

        userName = s.getString("username", "abc");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();

        FloatingActionButton fab = findViewById(R.id.fabFolders);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(FoldersActivity.this, "Kliknuli ste fab", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(FoldersActivity.this, CreateFolderActivity.class);
                startActivity(intent);
            }
        });

        startRepeatingTask();




    }
    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            try {
                SharedPreferences pref = SaveSharedPreference.getPreferences(getApplicationContext());
                String syncTimeStr = pref.getString("refresh_rate", "1");
                Log.d("SYNC TIME", syncTimeStr);
                mInterval = TimeUnit.MINUTES.toMillis(Integer.parseInt(syncTimeStr));

                Toast toast = Toast.makeText(getApplicationContext(), "Syncing", Toast.LENGTH_SHORT);
                toast.show();


                FoldersInterface service = RetrofitClient.getClient().create(FoldersInterface.class);
                Call<ArrayList<Folder>> call = service.getAllFolders(userName);

                call.enqueue(new Callback<ArrayList<Folder>>() {
                    @Override
                    public void onResponse(Call<ArrayList<Folder>> call, Response<ArrayList<Folder>> response) {
                        folders = response.body();
                        FoldersAdapter foldersAdapter = new FoldersAdapter(FoldersActivity.this, R.layout.adapter_view_folders, folders);

                        ListView listViewFolders = findViewById(R.id.list_view_folders);
                        listViewFolders.setAdapter(foldersAdapter);

                        listViewFolders.setOnItemClickListener(new AdapterView.OnItemClickListener()
                        {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View  view, int position, long id)
                            {
                                Intent folder = new Intent(FoldersActivity.this, FolderActivity.class);
                                folder.putExtra("FOLDER", folders.get(position));

                                startActivity(folder);
                            }
                        });



                    }

                    @Override
                    public void onFailure(Call<ArrayList<Folder>> call, Throwable t) {
                        Log.d("sa", "asa");
                        Log.d("sas", t.getMessage());
                        Log.d("sss", t.getStackTrace().toString());
                        if (t instanceof IOException) {
                            Toast.makeText(FoldersActivity.this, "this is an actual network failure :( inform the user and possibly retry", Toast.LENGTH_SHORT).show();
                            // logging probably not necessary
                        }
                        else {
                            Toast.makeText(FoldersActivity.this, "conversion issue! big problems :(", Toast.LENGTH_SHORT).show();
                            // todo log to some central bug tracking service
                        }
                        Toast.makeText(FoldersActivity.this ,"sss", Toast.LENGTH_LONG);
                    }
                });


            } finally {
                mHandler.postDelayed(mStatusChecker, mInterval);
            }
        }
    };
    void startRepeatingTask() {
        mStatusChecker.run();
    }

    void stopRepeatingTask(){
        mHandler.removeCallbacks(mStatusChecker);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHandler = new Handler();
        setContentView(R.layout.activity_folders);

        Toolbar tool=findViewById(R.id.toolbarFolders);
        setSupportActionBar(tool);
        getSupportActionBar().setTitle("Folders");

        drawer=findViewById(R.id.drawer_folders);

        toggle=new ActionBarDrawerToggle(this,drawer,tool,R.string.drawable_layout_otvori,R.string.drawable_layout_zatvori);
        Log.d("S", "Sss" + drawer + "aaa" + toggle);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navView = findViewById(R.id.nav_folders);
        navView.setNavigationItemSelectedListener(this);
        MenuItem currentMenu = navView.getMenu().findItem(R.id.folders);
        currentMenu.setChecked(true);

        View header=navView.getHeaderView(0);
        header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent setings = new Intent(FoldersActivity.this, ProfileActivity.class);
                startActivity(setings);
            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        switch (menuItem.getItemId()) {

            case R.id.profile: {
                Intent profile = new Intent(FoldersActivity.this,ContactsActivity.class);
                startActivity(profile);
                break;
            }
            case R.id.folders: {
                drawer.closeDrawers();
                break;
            }
            case R.id.settings: {
                Intent seti = new Intent(FoldersActivity.this, SettingsActivity.class);
                startActivity(seti);
                break;
            }
            case R.id.fav: {
                Intent folders = new Intent(FoldersActivity.this, TagActivity2.class);
                startActivity(folders);
                break;
            }
            case R.id.lik: {
                Intent folders = new Intent(FoldersActivity.this, TagActivity.class);
                startActivity(folders);
                break;
            }
            case R.id.emails: {
                Intent emails = new Intent(FoldersActivity.this, EmailsActivity.class);
                startActivity(emails);
                break;
            }
            case R.id.odjava: {
                SaveSharedPreference.getLoggedStatus(getApplicationContext());
                break;
            }





        }
        return true;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.folders_toolbar,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id=item.getItemId();
        if(id==R.id.btnAddFolder){
            Toast.makeText(FoldersActivity.this, "Novi folder", Toast.LENGTH_LONG).show();
            Intent profile = new Intent(FoldersActivity.this, CreateFolderActivity.class);
            startActivity(profile);
        }


        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onPause() {
        super.onPause();
        stopRepeatingTask();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
