package com.example.projekatandroid;

import android.Manifest;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Environment;
import android.os.StrictMode;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.io.FileUtils;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import adapteri.AttachmentForEmailAdapter;
import adapteri.EmailAdapter;
import adapteri.FoldersAdapter;
import model.Account;
import model.Attachment;
import model.Contact;
import model.Folder;
import model.Message;
import model.Tag;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import servisi.ContactsInterface;
import servisi.MessagesInterface;
import servisi.RetrofitClient;

public class EmailActivity extends AppCompatActivity {
    private List<Attachment> attachments;
    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 1;

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();





        Intent izEmailsa = getIntent();

        Message message = (Message) izEmailsa.getExtras().getSerializable("EMAIL");

        Log.d("folder.getId()","fdsd"+message.getId());
        MessagesInterface service = RetrofitClient.getClient().create(MessagesInterface.class);

        for (int i=0;i<message.getTags().size();i++){
            if(message.getTags() != null){
                if(message.getTags().get(i).getName().equals("favorite")){
                    MenuItem Mi=(MenuItem)findViewById(R.id.favorite);
                    Mi.setTitle("Y");
                    Drawable drawable = Mi.getIcon();
                    drawable.mutate();
                    drawable.setColorFilter(getResources().getColor(R.color.color2), PorterDuff.Mode.SRC_IN);
                }
                else if(message.getTags().get(i).getName().equals("like")){
                    MenuItem Mi=(MenuItem)findViewById(R.id.like);
                    Drawable drawable = Mi.getIcon();
                    drawable.mutate();
                    drawable.setColorFilter(getResources().getColor(R.color.color10), PorterDuff.Mode.SRC_IN);
                }
            }
        }

        TextView naslovZaEmail = findViewById(R.id.naslovZaEmail);

        naslovZaEmail.setText(message.getSubject());

        TextView odEmail = findViewById(R.id.odEmail);

        odEmail.setText(message.getFrom());


        TextView porukaEmail = findViewById(R.id.porukaEmail);
        porukaEmail.setText(message.getContent());


        attachments = message.getAttachments();
        AttachmentForEmailAdapter attachmentsAdapter = new AttachmentForEmailAdapter(this, R.layout.adapter_view_attachment, attachments);
        ListView listViewAttachments = findViewById(R.id.listAtachmantEmail);
        listViewAttachments.setAdapter(attachmentsAdapter);
        listViewAttachments.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Toast.makeText(EmailActivity.this, "Ajde", Toast.LENGTH_LONG).show();
                Attachment attachment = message.getAttachments().get(position);

                FileOutputStream outputStream = null;



                try {
                    if (ContextCompat.checkSelfPermission(getApplicationContext(),
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        if (!(ActivityCompat.shouldShowRequestPermissionRationale(EmailActivity.this,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE)))
                            ActivityCompat.requestPermissions(EmailActivity.this,
                                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
                    }
                    outputStream = openFileOutput(attachment.getName(), MODE_PRIVATE);
                    outputStream.write(Base64.decode(attachment.getData(), Base64.DEFAULT));

                    //todo obrisi
//                    outputStream = openFileOutput("jova.txt", MODE_PRIVATE);
//                    outputStream.write("Hello world".getBytes());


//                    FileInputStream fis = null;
//                    fis = openFileInput("jova.txt");
//                    InputStreamReader isr = new InputStreamReader(fis);
//                    BufferedReader br = new BufferedReader(isr);
//                    StringBuilder sb = new StringBuilder();
//                    String text;
//                    while((text = br.readLine()) != null){
//                        sb.append(text).append("/n");
//                    }
//                    Log.d("Pera:", sb.toString());
//                    fis.close();
//                    //todo obrisi

                    outputStream.close();
                    File attachedFile = new File(getFilesDir().getAbsolutePath(), attachment.getName());
                    attachedFile.setReadable(true, false);
                    File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                    File file = new File(dir + File.separator +  attachment.getName());
                    FileUtils.copyFile(attachedFile, file, true);            Log.i("Download", "Done");
                    Log.i("Src", attachedFile.getAbsolutePath());
                    Log.i("File", file.getAbsolutePath());

                    openFile(file);
                }catch (IOException e){
                    e.printStackTrace();
                } finally {
                    if (outputStream != null){
                        try {
                            outputStream.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }


            }
        });


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder(); StrictMode.setVmPolicy(builder.build());
        setContentView(R.layout.activity_email);

        Toolbar toolbar = findViewById(R.id.toolbarEmail);
        setSupportActionBar(toolbar);
        toolbar.inflateMenu(R.menu.email);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.email,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id=item.getItemId();
        if(id==R.id.btnBackFromCreateEmailToEmails){
            Toast.makeText(EmailActivity.this, "Kliknuli ste back dugme EmailActivity", Toast.LENGTH_LONG).show();
            Intent profile = new Intent(EmailActivity.this, EmailsActivity.class);
            startActivity(profile);
        }

        if(id==R.id.btndelete){
            Intent izEmailsa = getIntent();
            Message folder = (Message) izEmailsa.getExtras().getSerializable("EMAIL");
            MessagesInterface service = RetrofitClient.getClient().create(MessagesInterface.class);
            Call<Void> call = service.getDeleteMessages(folder.getId());
            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    Intent i=new Intent(EmailActivity.this, EmailsActivity.class);
                    startActivity(i);
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    Toast.makeText(EmailActivity.this, "Something went wrong...Please try later!", Toast.LENGTH_SHORT);
                }
            });

//            Toast.makeText(EmailActivity.this, "Kliknuli ste delete dugme EmailActivity", Toast.LENGTH_LONG).show();
        }
        if(id==R.id.btnreply){
            Intent izEmailsa = getIntent();
            Message folder = (Message) izEmailsa.getExtras().getSerializable("EMAIL");
            Intent i=new Intent(EmailActivity.this, CreateEmailActivity.class);
            i.putExtra("Email",folder);
            startActivity(i);
            Toast.makeText(EmailActivity.this, "Kliknuli ste reply dugme EmailActivity", Toast.LENGTH_LONG).show();
        }
        if(id==R.id.saveAttachment){

            Toast.makeText(EmailActivity.this, "Kliknuli ste save Attacgmant dugme EmailActivity", Toast.LENGTH_LONG).show();
        }if(id==R.id.btnForward){
            Intent izEmailsa = getIntent();
            Message folder = (Message) izEmailsa.getExtras().getSerializable("EMAIL");
            Intent i=new Intent(EmailActivity.this, CreateEmailActivity.class);
            i.putExtra("Email1",folder);
            startActivity(i);
            Toast.makeText(EmailActivity.this, "Kliknuli ste save Attacgmant dugme EmailActivity", Toast.LENGTH_LONG).show();
        }
        if(id==R.id.btnreplyAll){

            Toast.makeText(EmailActivity.this, "Kliknuli ste replyAll dugme EmailActivity", Toast.LENGTH_LONG).show();
        }
        if(id==R.id.favorite){


                Drawable drawable = item.getIcon();
                item.setTitle("Y");
                drawable.mutate();
                drawable.setColorFilter(getResources().getColor(R.color.color2), PorterDuff.Mode.SRC_IN);

                Tag tag=new Tag(hashCode(),"favorite",null);
                Intent izEmailsa = getIntent();
                Message folder = (Message) izEmailsa.getExtras().getSerializable("EMAIL");
                folder.getTags().add(tag);

                MessagesInterface service = RetrofitClient.getClient().create(MessagesInterface.class);
                Call<Void> call = service.getMessages(folder.getId(),"favorite",true);
                call.enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        Toast.makeText(EmailActivity.this, "Dodato u favorite tag!", Toast.LENGTH_SHORT);
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        Toast.makeText(EmailActivity.this, "Something went wrong...Please try later!", Toast.LENGTH_SHORT);
                    }
                });



//            if(item.getTitle().equals("Y")) {
//                Drawable drawable = item.getIcon();
//                item.setTitle("N");
//                drawable.mutate();
//                drawable.setColorFilter(getResources().getColor(R.color.color11), PorterDuff.Mode.SRC_IN);
//
//                Tag tag=new Tag(hashCode(),"favorite",null);
//                Intent izEmailsa = getIntent();
//                Message folder = (Message) izEmailsa.getExtras().getSerializable("EMAIL");
//                folder.getTags().add(tag);
//                for (Tag t:folder.getTags()) {
//                    if(t.getName().equals("favorite"))
//                        tag=t;
//                }
//                if(tag != null)
//                    folder.getTags().remove(tag);
//            }

        }
        if(id==R.id.like){

                Drawable drawable = item.getIcon();
                drawable.mutate();
                drawable.setColorFilter(getResources().getColor(R.color.color10), PorterDuff.Mode.SRC_IN);
                Tag tag = new Tag(hashCode(), "like", null);
                Intent izEmailsa = getIntent();
                Message folder = (Message) izEmailsa.getExtras().getSerializable("EMAIL");
                folder.getTags().add(tag);

                MessagesInterface service = RetrofitClient.getClient().create(MessagesInterface.class);
                Call<Void> call = service.getMessages(folder.getId(),"favorite",true);
                call.enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        Toast.makeText(EmailActivity.this, "Dodato u favorite tag!", Toast.LENGTH_SHORT);
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        Toast.makeText(EmailActivity.this, "Something went wrong...Please try later!", Toast.LENGTH_SHORT);
                    }
                });



        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }



    private void openFile(File url) {

        try {
            Uri uri = Uri.fromFile(url);
            //ajde
            Log.d("PLATE", url.toString());

            Toast.makeText(EmailActivity.this, url.toString(), Toast.LENGTH_LONG).show();

            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            if (url.toString().contains(".doc") || url.toString().contains(".docx")) {
                // Word document
                intent.setDataAndType(uri, "application/msword");
            } else if (url.toString().contains(".pdf")) {
                Toast.makeText(EmailActivity.this, "Ajde5", Toast.LENGTH_LONG).show();
                // PDF file
                intent.setDataAndType(uri, "application/pdf");
            } else if (url.toString().contains(".ppt") || url.toString().contains(".pptx")) {
                Toast.makeText(EmailActivity.this, "Ajde6", Toast.LENGTH_LONG).show();
                // Powerpoint file
                intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
            } else if (url.toString().contains(".xls") || url.toString().contains(".xlsx")) {

                // Excel file
                intent.setDataAndType(uri, "application/vnd.ms-excel");
            } else if (url.toString().contains(".zip") || url.toString().contains(".rar")) {
                // WAV audio file
                intent.setDataAndType(uri, "application/x-wav");
            } else if (url.toString().contains(".rtf")) {
                // RTF file
                intent.setDataAndType(uri, "application/rtf");
            } else if (url.toString().contains(".wav") || url.toString().contains(".mp3")) {
                // WAV audio file
                intent.setDataAndType(uri, "audio/x-wav");
            } else if (url.toString().contains(".gif")) {
                // GIF file
                intent.setDataAndType(uri, "image/gif");
            } else if (url.toString().contains(".jpg") || url.toString().contains(".jpeg") || url.toString().contains(".png")) {
                // JPG file
                intent.setDataAndType(uri, "image/jpeg");
            } else if (url.toString().contains(".txt")) {
                // Text file
                intent.setDataAndType(uri, "text/plain");
            } else if (url.toString().contains(".3gp") || url.toString().contains(".mpg") ||
                    url.toString().contains(".mpeg") || url.toString().contains(".mpe") || url.toString().contains(".mp4") || url.toString().contains(".avi")) {
                // Video files
                intent.setDataAndType(uri, "video/*");
            } else {
                intent.setDataAndType(uri, "*/*");
            }
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            EmailActivity.this.startActivity(intent);

        } catch (ActivityNotFoundException e) {
            Toast.makeText(EmailActivity.this, "No application found which can open the file", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

}
