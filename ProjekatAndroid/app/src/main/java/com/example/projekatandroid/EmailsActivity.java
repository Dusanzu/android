package com.example.projekatandroid;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Handler;
import android.preference.CheckBoxPreference;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;


import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;


import adapteri.AttachmentForEmailAdapter;
import adapteri.EmailAdapter;
import model.Account;
import model.Attachment;
import model.Contact;
import model.Message;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import servisi.MessagesInterface;
import servisi.RetrofitClient;
import servisi.SaveSharedPreference;




public class EmailsActivity extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener,SearchView.OnQueryTextListener {
    private Message message;
    private DrawerLayout drawer;
    private ActionBarDrawerToggle toggle;
    private ListView listView;
    private ListView listView2;
    private EmailAdapter mAdapter;
    private AttachmentForEmailAdapter mAdapter2;
    private ArrayList<Message> messages;
    private ArrayList<Attachment> attachments;
    ProgressDialog progressDoalog;
    private CheckBox ch,ch1;

    private long mInterval = 0;
    private Handler mHandler;

    private String userName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emails);

        SharedPreferences s = SaveSharedPreference.getPreferences(getApplicationContext());

        userName = s.getString("username", "abc");
        Log.d("sdf","username: "+userName);
        mHandler = new Handler();


        Log.d("CREATION"," pre ulaza  fffff");




        Toolbar tool=findViewById(R.id.toolbarCreateEmail);
        setSupportActionBar(tool);

        drawer=findViewById(R.id.drawer_create_email);

        toggle=new ActionBarDrawerToggle(this,drawer,tool,R.string.drawable_layout_otvori,R.string.drawable_layout_zatvori);

        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navView = findViewById(R.id.nav_view_emails);
        navView.setNavigationItemSelectedListener(this);

        View header=navView.getHeaderView(0);
        header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent setings1 = new Intent(EmailsActivity.this, ProfileActivity.class);
                startActivity(setings1);
            }
        });

//        TextView v=findViewById(R.id.Icon);
//        int color = Color.rgb( 205, 127, 50);
//        ((GradientDrawable)v.getBackground()).setColor(color);

        FloatingActionButton fab = findViewById(R.id.fabEmails1);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent setings = new Intent(EmailsActivity.this, CreateEmailActivity.class);
                setings.putExtra("EMAIL3", "Dusan");
                startActivity(setings);
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.emails,menu);
        MenuItem search=menu.findItem(R.id.search);
        SearchView se=(SearchView)search.getActionView();
        se.setOnQueryTextListener(this);
        return true;
    }

    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            try {
                SharedPreferences pref = SaveSharedPreference.getPreferences(getApplicationContext());

                String syncTimeStr = pref.getString("refresh_rate", "1");
                Log.d("SYNC TIME", syncTimeStr);
                //SettingsActivity.vreme= Integer.parseInt(syncTimeStr);
                mInterval = TimeUnit.MINUTES.toMillis(Integer.parseInt(syncTimeStr));

                Toast toast = Toast.makeText(getApplicationContext(), "Syncing", Toast.LENGTH_SHORT);
                toast.show();

                MessagesInterface service = RetrofitClient.getClient().create(MessagesInterface.class);
                Call<ArrayList<Message>> call = service.getAllMessages(userName);
                call.enqueue(new Callback<ArrayList<Message>>() {
                    @Override
                    public void onResponse(Call<ArrayList<Message>> call, Response<ArrayList<Message>> response) {
//                progressDoalog.dismiss();



                        messages= response.body();

                        Log.d("naslov13",String.valueOf(response.body()));
//                        messages.get(1);
//                        Log.d("naslov13", String.valueOf(messages.get(1)));
                        listView =(ListView)findViewById(R.id.emailLista);
                        mAdapter=new EmailAdapter(EmailsActivity.this,messages.size(),messages);


                        SharedPreferences prefs = getSharedPreferences("pogledano", MODE_PRIVATE);
                        String restoredText = prefs.getString("posiljalac", null);
                        Log.d("2name22+1+","dd");
                        if (restoredText != null) {
                            String posiljalac = prefs.getString("posiljalac", "No name defined");//"No name defined" is the default value.
                            String poruka = prefs.getString("poruka", "No name defined");//"No name defined" is the default value.
                            String naslov = prefs.getString("naslov", "zaustavi");//"No name defined" is the default value.
                            String pozicija = prefs.getString("pozicija8", "1");//"No name defined" is the default value.
                            String position = prefs.getString("position", "0");//"No name defined" is the default value.



                            Log.d("pozicija", pozicija);
                            Log.d("posiljalac", posiljalac);
                            Log.d("poruka", poruka);
                            Log.d("naslov", naslov);
                          //  posiljalac="dalje";


                            if(EmailAdapter.brojac==0)
                            {
                                naslov= "zaustavi";
                            }

                            if(posiljalac!=null)
                            {
                                if(!naslov.equals("zaustavi")) {
                                    EmailAdapter.brojac = 0;
                                    notificationall(posiljalac, messages.get(Integer.parseInt(position)));
                                }
                            }
                        }
                        Log.d("2name22+2+","dd");

                        listView.setAdapter(mAdapter);

                        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
                        {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View  view, int position, long id)
                            {
                                Log.d("naslov13",String.valueOf(position));
                                Intent setings = new Intent(EmailsActivity.this, EmailActivity.class);
                                setings.putExtra("EMAIL", messages.get(position));
                                startActivity(setings);
                            }
                        });
                    }
                    @Override
                    public void onFailure(Call<ArrayList<Message>> call, Throwable t) {
//                progressDoalog.dismiss();
                        Log.d("dadsda","fdsd");
                        Log.d("asssss",t.getMessage());
                        Log.d("dgdh",t.getStackTrace().toString());
                        Toast.makeText(EmailsActivity.this, "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
                    }
                });

            }finally {
                mHandler.postDelayed(mStatusChecker, mInterval);
            }
        }
    };

    void startRepeatingTask() {
        mStatusChecker.run();
    }

    void stopRepeatingTask(){
        mHandler.removeCallbacks(mStatusChecker);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        switch (menuItem.getItemId()) {

            case R.id.profile: {
                    Intent profile = new Intent(EmailsActivity.this,ContactsActivity.class);
                startActivity(profile);
                break;
            }
            case R.id.folders: {
                Intent folders = new Intent(EmailsActivity.this, FoldersActivity.class);
                startActivity(folders);
                break;
            }
            case R.id.fav: {
                Intent folders = new Intent(EmailsActivity.this, TagActivity2.class);
                startActivity(folders);
                break;
            }
            case R.id.lik: {
                Intent folders = new Intent(EmailsActivity.this, TagActivity.class);
                startActivity(folders);
                break;
            }
            case R.id.settings: {
                Intent setings = new Intent(EmailsActivity.this, SettingsActivity.class);
                startActivity(setings);
                break;
            }
            case R.id.emails: {
                Intent setings = new Intent(EmailsActivity.this, EmailsActivity.class);
                startActivity(setings);
                break;
            }

            case R.id.odjava: {
                SaveSharedPreference.getLoggedStatus(getApplicationContext());
                break;
            }


        }
        return true;
    }



    @Override
    public void onBackPressed() {
        if(drawer.isDrawerOpen(GravityCompat.START)){
            drawer.closeDrawer(GravityCompat.START);
        }else {
            super.onBackPressed();
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        startRepeatingTask();

    }
    @Override
    protected void onPause() {
        super.onPause();
        stopRepeatingTask();

    }

    @Override
    protected void onStop() {
        super.onStop();



    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

//    public void proveraa(ArrayList<Message> emailList){
//        for (Message i:emailList) {
//            if(i.isProcitano()){
//                notificationall();
//            }
//        }
//    }




    public  void notificationall(String posiljalac,Message mes){
//        Log.d("position12",String.valueOf(position));
//        if(position.equals(" nova porka koja je upravo stigla"))
//            position="0";
//        Integer pozicija1=Integer.parseInt(position);
        if(posiljalac.equals("vise")) {


            Intent resultIintent = new Intent(this, EmailsActivity.class);
            PendingIntent resultPedingItent = PendingIntent.getActivity(this, 1, resultIintent, PendingIntent.FLAG_UPDATE_CURRENT);

            NotificationCompat.Builder notificationbilder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                    .setDefaults(NotificationCompat.DEFAULT_ALL).setSmallIcon(R.drawable.malaikona)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.velikaslika))
                    .setContentTitle("Pristigle poruke")
                    .setContentText("ff")
                    .setAutoCancel(true)
                    .setContentIntent(resultPedingItent);

            NotificationManager notificationMenaget = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationMenaget.notify(1, notificationbilder.build());

        }

        else{
            Intent resultIintent = new Intent(this, EmailActivity.class);
//            MessagesInterface service = RetrofitClient.getClient().create(MessagesInterface.class);
//            Call<ArrayList<Message>> call = service.getAllMessages();


           resultIintent.putExtra("EMAIL", mes);

            PendingIntent resultPedingItent = PendingIntent.getActivity(this, 1, resultIintent, PendingIntent.FLAG_UPDATE_CURRENT);

            NotificationCompat.Builder notificationbilder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                    .setDefaults(NotificationCompat.DEFAULT_ALL).setSmallIcon(R.drawable.malaikona)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ee))
                    .setContentTitle("Poruka")
                    .setContentText(mes.getFrom())
                    .setAutoCancel(true)
                    .setContentIntent(resultPedingItent);

            NotificationManager notificationMenaget = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationMenaget.notify(1, notificationbilder.build());





        }




    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        String string=newText.toLowerCase();
        List<Message>poruke=new ArrayList<>();
        for (Message m:messages) {
            if(m.getFrom().toLowerCase().contains(string)){
                poruke.add(m);
            }
            else if(m.getSubject().toLowerCase().contains(string)){
                poruke.add(m);
            }
            else if(m.getTo().get(0).toLowerCase().contains(string)){
                poruke.add(m);
            }
        }
        mAdapter.update(poruke);
        return false;
    }
}

