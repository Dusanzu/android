package com.example.projekatandroid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import servisi.AcountService;

import model.Account;
import servisi.RetrofitClient;
import servisi.SaveSharedPreference;

public class EditProfileActivity extends AppCompatActivity {
    private Account account = new Account();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        Toolbar tool = findViewById(R.id.toolbar_edit_profile);
        setSupportActionBar(tool);

        getSupportActionBar().setTitle("Edit profile");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id=item.getItemId();

        if (id == android.R.id.home ){
            finish();
        }


        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onResume() {
        super.onResume();

        account = (Account) getIntent().getSerializableExtra("PROFILE");

        EditText editTexProfileUsername  = findViewById(R.id.editTextProfileUsername);
        EditText editTextProfilePassword = findViewById(R.id.editTextProfilePassword);
        EditText EditTextSmtpEdit = findViewById(R.id.EditTextSmtpEdit);

        editTexProfileUsername.setText(account.getUsername());
        editTextProfilePassword.setText(account.getPassword());
        EditTextSmtpEdit.setText(account.getSmtp());
        switch (account.getPop3Imap()){
            case "pop3":
                RadioButton radioButton = findViewById(R.id.radio_pirates);
                radioButton.setChecked(true);
                break;
            case "imap":
                RadioButton radioButton1 = findViewById(R.id.radio_ninjas);
                radioButton1.setChecked(true);
                break;
            default:
                account.setPop3Imap("imap");
                RadioButton radioButton2 = findViewById(R.id.radio_ninjas);
                radioButton2.setChecked(true);
                break;
        }
        Button btnIzmeniEditProfile = findViewById(R.id.btnIzmeniEditProfile);
        btnIzmeniEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTexProfileUsername.getText().toString().equals("") || editTextProfilePassword.getText().toString().equals("") ||
                        EditTextSmtpEdit.getText().toString().equals("") || account.getPop3Imap().equals("")) {
                    Toast.makeText(EditProfileActivity.this, "Niste uneli sve podatke", Toast.LENGTH_SHORT);
                }
                else {
                    account.setUsername(editTexProfileUsername.getText().toString());
                    account.setPassword(editTextProfilePassword.getText().toString());
                    account.setSmtp(EditTextSmtpEdit.getText().toString());
                    AcountService accountService = RetrofitClient.getClient().create(AcountService.class);
                    Call<Account> accountCall = accountService.editAccount(account, account.getId());
                    accountCall.enqueue(new Callback<Account>() {
                        @Override
                        public void onResponse(Call<Account> call, Response<Account> response) {
                            account = response.body();
                            //TODO Shared preference should be here
                            SaveSharedPreference.setLoggedIn(getApplicationContext(), account);
                            //
                            finish();
                        }

                        @Override
                        public void onFailure(Call<Account> call, Throwable t) {
                            Toast.makeText(EditProfileActivity.this,"Nesto nije uspelo", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });

    }
    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radio_pirates:
                if (checked)
                    account.setPop3Imap("pop3");
                    break;
            case R.id.radio_ninjas:
                if (checked)
                    account.setPop3Imap("imap");
                    break;
        }
    }
}
