package com.example.projekatandroid;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

import model.Account;
import model.Message;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import servisi.AcountService;
import servisi.RetrofitClient;
//import servisi.SaveSharedPreference;
import servisi.SaveSharedPreference;
import servisi.ServiceUtilise;
import tools.CurrentUser;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TASK;

public class LoginActivity extends AppCompatActivity {

    EditText edtUsername;
    EditText edtPassword;
    Button btnLogin;
    AcountService service;
    CurrentUser currentUser;
    private ArrayList<Account> accounts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }
    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        edtUsername = (EditText) findViewById(R.id.editText);
        edtPassword = (EditText) findViewById(R.id.editText2);
//        service = ServiceUtilise.getAcountService();
        Button btnRegistration=(Button)findViewById(R.id.btnRegistration);
        btnRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, CreateAccount.class);
                startActivity(intent);
            }
        });

        Button btnLogin=(Button)findViewById(R.id.btnStart);

                btnLogin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                String username = edtUsername.getText().toString();
                String password = edtPassword.getText().toString();

                Retrofit retrofit = RetrofitClient.getClient();
                AcountService services = retrofit.create(AcountService.class);
                Call<Account> call = services.login(username,password);
                call.enqueue(new Callback<Account>() {
                    @Override
                    public void onResponse(Call<Account> call, Response<Account> response) {
                        if(response.isSuccessful()){
//<<<<<<< HEAD
//                            accounts= response.body();
//                            Log.d("dadsda",accounts.toString());
//                            for (Account a:accounts){
//                                if(a.getUsername().equals(username) && a.getPassword().equals(password)){
//                                    SaveSharedPreference.setLoggedIn(getApplicationContext(), a);
//                                    //login start main activity
//                                    Intent intent = new Intent(LoginActivity.this, EmailsActivity.class);
//                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
////                       intent.putExtra("username", username);
//                                    startActivity(intent);
//                                    finish();
//                                    break;
//                                }
//=======
                            Account a= response.body();
//>>>>>>> c857134e22feac43f8d7b3592ba7e861a80d1161

                            SaveSharedPreference.setLoggedIn(getApplicationContext(), a);
                            SaveSharedPreference.setRefreshrate(getApplicationContext(), "1");
                            //login start main activity
                            Intent intent = new Intent(LoginActivity.this, EmailsActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                           intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK |FLAG_ACTIVITY_CLEAR_TASK);
                         //   intent.putExtra("token", a.getToken());
                            startActivity(intent);
                            finish();

                        }
                        Toast.makeText(LoginActivity.this, "The username or password is incorrect", Toast.LENGTH_SHORT).show();

                    }


                    @Override
                    public void onFailure(Call call, Throwable t) {
                        Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Log.d("dadsda","fdsd");
                        Log.d("sdfdsfsd",t.getMessage());
                        Log.d("sdfdsfsd",t.getStackTrace().toString());

                    }
                });

//                //validate form
//                if(validateLogin(username, password)==true){
////                    do login
//                    doLogin(username, password);
//                }
                Toast.makeText(LoginActivity.this, "The username or password is incorrect", Toast.LENGTH_SHORT).show();
//                Intent emailsActivity = new Intent(LoginActivity.this,EmailsActivity.class);
//                startActivity(emailsActivity);
            }
        });

    }

    private void doLogin(final String username, final String password){

    }
//
    public void logout() {
//        Retrofit retrofit = RetrofitClient.getClient();
//        AcountService loginServices = retrofit.create(AcountService.class);
//        Call<Void> logout = loginServices.logout();
//
//        logout.enqueue(new Callback<Void>() {
//            @Override
//            public void onResponse(Call<Void> call, Response<Void> response) {
//                if (response.code() == 200) {
//                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(intent);
//                }
//            }
//
//            @Override
//            public void onFailure(Call<Void> call, Throwable t) {
//                Log.e("TAG", "=======onFailure: " + t.toString());
//                t.printStackTrace();
//            }
//        });


    }


    private boolean validateLogin(String edtUsername, String edtPassword){
        if(edtUsername == null || edtUsername.trim().length() == 0){
            Toast.makeText(this, "Username is required", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(edtPassword == null || edtPassword.trim().length() == 0){
            Toast.makeText(this, "Password is required", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }



    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
