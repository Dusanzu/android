package tools;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.example.projekatandroid.R;
import com.example.projekatandroid.SplashActivity;

import java.security.acl.Group;
import java.util.ArrayList;
import java.util.List;

import model.Account;
import model.Message;

public class CurrentUser {
    public static boolean exists(Context context)
    {
        return getId(context) != null;
    }

    public static String getName(Context context)
    {
        return getProfileData(context, R.string.current_user_name);
    }



    public static String getId(Context context)
    {
        return getProfileData(context, R.string.current_user_id);
    }

    private static String getProfileData(Context context, int id)
    {
        return getSharedPreferences(context).getString(context.getString(id), null);
    }
//    private static ArrayList<Message> getMessage(Context context,ArrayList<Message> messages){
//        return getSharedPreferences(context).getAll(context.get)
//    }
//    public List<Message> getGroupsCreated()
//    {
//    return getMany(Group.class, "userCreated");
//    }

//    public static Account getModel(Context context)
////    {
//////        return Account.getEma(getEmail(context));
////    }

    public static void login(Account user, Context context)
    {
        getSharedPreferences(context).edit()


                .putString(context.getString(R.string.current_user_name), user.getUsername())
                .commit();
        Log.d("LOGIN",
                "logovo se sa id: " + user.getId() +
                        " useraname: " + user.getUsername());

    }

    public static void logout(Context context)
    {
        getSharedPreferences(context).edit()
                .putString(context.getString(R.string.current_user_id), null)

                .putString(context.getString(R.string.current_user_name), null)
                .commit();
        context.startActivity(new Intent(context, SplashActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
    }

    private static SharedPreferences getSharedPreferences(Context context)
    {
        return context.getSharedPreferences(context.getString(R.string.current_user_preferences), Context.MODE_PRIVATE);
    }
}
